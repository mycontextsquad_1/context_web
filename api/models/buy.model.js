const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let buySchema = new Schema({
    facilitatorAccessToken: {
        type: String
    },
    orderID: {
        type: String
    },
    payerID: {
        type: String
    },
    price: {
        type: Number
    },
    recordId: {
        type: String
    },
    userId: {
        type: String
    },
});


module.exports = mongoose.model("myrecords", buySchema, "context-myrecords");
