const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let buySchema = new Schema({
    userId: {
        type: String,
    },
    hasPurchased: {
        type: Boolean,
        default: false
    },
    noOfUsersSubmitted: {
        type: Number,
        default: 0
    },
    purchasedFrom: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user"
    },
    requestedBy: {
        type: [String],
        default: []
    },
    description: {
        type: String,
    },
    cancerType: {
        type: String,
    },
    tumorSize: {
        type: Number,
    },
    ageGroup: {
        type: String,
    },
    price: {
        type: Number,
    },
    gender: {
        type: String,
        enum: ["Male", "Female"],
        default: "Male"
    },
    startDate: {
        type: Date,
        default: Date()
    },
    endDate: {
        type: Date,
        default: Date()
    }

});


module.exports = mongoose.model("offers", buySchema, "context-offers");
