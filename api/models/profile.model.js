const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let profileSchema = new Schema({
    organization: {
        type: String
    },
    designation: {
        type: String
    },
    phone: {
        type: String
    },
    address: {
        type: String
    },
    userId: {
        type: String
    },
});


module.exports = mongoose.model("profile", profileSchema, "profile-details");
