const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let requestAccessSchema = new Schema({
    doctorId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "user"
    },
    patientId: {
        type: Schema.Types.ObjectId,
        required: true,
        ref: "user"
    },
    hasAccepted: {
        type: Boolean,
        default: false
    },
});


module.exports = mongoose.model("requestAccess", requestAccessSchema, "doctor-request-access");
