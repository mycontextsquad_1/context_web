const mongoose = require("mongoose");
const Schema = mongoose.Schema;

let buySchema = new Schema({
    userId: {
        type: Schema.Types.ObjectId,
        ref: "user"
    },
    offerId: {
        type: Schema.Types.ObjectId,
        ref: "offers"
    },
    price: {
        type: Number,
    },
});


module.exports = mongoose.model("offers-submissions", buySchema, "context-offers-submission");
