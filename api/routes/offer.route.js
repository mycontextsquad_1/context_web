const express = require("express");
const offer_controller = require("../controllers/offer.controller");
var VerifyToken = require("./VerifyToken");

const router = express.Router();

router.put("/add-offer", VerifyToken, offer_controller.addOffer);
router.get("/my-offers", VerifyToken, offer_controller.getMyOffers);
router.patch("/update-offer", VerifyToken, offer_controller.updateOffer);
router.delete("/:id", VerifyToken, offer_controller.deleteOffer);
router.post("/patient-offers", VerifyToken, offer_controller.getOffersForPatients);
router.put("/send-request", VerifyToken, offer_controller.sendRecordForOffer);
router.get("/:id/submissions", VerifyToken, offer_controller.getSubmissionsForOffer);
router.get("/:id/buy", VerifyToken, offer_controller.buyOfferFromPatient);

module.exports = router;
