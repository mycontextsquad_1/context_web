const express = require("express");
const user_controller = require("../controllers/user.controller");
var VerifyToken = require("./VerifyToken");

const router = express.Router();

router.post("/list-users", VerifyToken, user_controller.listUsers);
router.post("/admin-users", VerifyToken, user_controller.listAllUsersForAdmin);
router.post("/buy-record", VerifyToken, user_controller.buyRecord);
router.post("/register", user_controller.register);
router.post("/login", user_controller.login);
router.get('/confirmation/:token', user_controller.confirmation);
router.get('/profile-details', VerifyToken, user_controller.getUserProfileDetails);
router.get('/wallet-details', VerifyToken, user_controller.getUserWalletDetails);
router.patch('/wallet-details', VerifyToken, user_controller.updateUserWalletDetails);
router.patch('/profile-details', VerifyToken, user_controller.updateUserProfileDetails);


router.get("/doctor-request", VerifyToken, user_controller.getDoctorRequests);
router.put("/doctor-request/patient", VerifyToken, user_controller.requestPatientForAccess);
router.get("/doctor-request/patient", VerifyToken, user_controller.getPatientRequestByDoctors);
router.get("/doctor-request/patient-accepted", VerifyToken, user_controller.getDoctorsApprovedPatients);
router.put("/doctor-request/patient/accept", VerifyToken, user_controller.acceptDoctorRequest);



module.exports = router;
