const Offer = require("../models/offer.model");
const OfferSubmission = require("../models/offer-submission.model");
/* Make an offer module  */

exports.addOffer = function (req, res) {
    let offerDetails = new Offer();
    let data = req.body;
    let startDate = new Date(data.startDate);
    startDate.setHours(0, 0, 0, 0);
    let endDate = new Date(data.endDate);
    endDate.setHours(23, 59, 59, 59);
    offerDetails.userId = data.userId;
    offerDetails.description = data.description;
    offerDetails.cancerType = data.cancerType;
    offerDetails.tumorSize = Number(data.tumorSize);
    offerDetails.ageGroup = data.ageGroup;
    offerDetails.price = data.price;
    offerDetails.gender = data.gender;
    offerDetails.startDate = startDate;
    offerDetails.endDate = endDate;
    offerDetails.save()
        .then(result => {
            res.json({
                success: true,
                message: "Offer added successfully.",
                data: result
            });
        })
        .catch(error => {
            res.json({
                success: false,
                message: "Failed to add offer."
            });
        });
}

exports.getMyOffers = function (req, res) {
    const userId = req.body.userId;
    console.log("userId", userId);
    let q = { userId: userId };
    switch (req.query.status) {
        case "new":
            q.noOfUsersSubmitted = 0;
            q.hasPurchased = false;
            break;
        case "pending":
            q.hasPurchased = false;
            q.noOfUsersSubmitted = { $gt: 0 };
            break;
        case "purchased":
            q.hasPurchased = true
            break;
    }
    Offer.find(q).populate("purchasedFrom", "-password")
        .then(result => {
            res.json({
                success: true,
                data: result,
                message: "my offers fetched"
            });
        })
        .catch(error => {
            res.json({
                success: false,
                message: "Unable to fetch my offers"
            });
        });
}

exports.getOffersForPatients = function (req, res) {
    let startDate = new Date();
    startDate.setHours(0, 0, 0, 0);
    let endDate = new Date();
    endDate.setHours(23, 59, 59, 59);
    let data = req.body;
    //startDate: { $lte: startDate }, endDate: { $gte: endDate } 
    let q = { hasPurchased: false };
    if (data.cancerType) {
        q.cancerType = data.cancerType;
    }
    if (data.tumorSize) {
        q.tumorSize = data.tumorSize;
    }
    if (data.ageGroup) {
        q.ageGroup = data.ageGroup;
    }
    if (data.gender) {
        q.gender = data.gender;
    }

    Offer.find(q)
        .then(result => {
            res.json({
                success: true,
                data: result,
                message: "offers fetched"
            });
        })
        .catch(error => {
            res.json({
                success: false,
                message: "Unable to fetch offers"
            });
        });
}


exports.updateOffer = function (req, res) {
    let data = req.body;
    let startDate = new Date(data.startDate);
    startDate.setHours(0, 0, 0, 0);
    let endDate = new Date(data.endDate);
    endDate.setHours(23, 59, 59, 59);
    data.startDate = startDate;
    data.endDate = endDate;
    Offer.findByIdAndUpdate(req.body._id, data)
        .then(result => {
            res.json({
                success: true,
                message: "Offer updated successfully."
            });
        })
        .catch(error => {
            res.json({
                success: false,
                message: "Failed to update offer."
            });
        });
};

exports.deleteOffer = function (req, res) {
    const id = req.params.id;
    Offer.findByIdAndDelete(id)
        .then(result => {
            res.json({
                success: true,
                message: "Offer deleted successfully."
            });
        })
        .catch(error => {
            res.json({
                success: false,
                message: "Failed to delete Offer"
            });
        });
};

exports.sendRecordForOffer = function (req, res) {
    const userId = req.body.userId;
    let data = req.body;
    let submittedUsers = data.requestedBy;
    submittedUsers.push(userId);
    Offer.findByIdAndUpdate(req.body._id, {
        $set: {
            noOfUsersSubmitted: Number(data.noOfUsersSubmitted) + 1,
            requestedBy: submittedUsers
        },

    }, { new: true })
        .then(result => {
            let offerSubmission = new OfferSubmission();
            offerSubmission.userId = data.userId;
            offerSubmission.offerId = data._id;
            offerSubmission.price = data.price;

            offerSubmission.save()
                .then(result => {
                    res.json({
                        success: true,
                        message: "Offer submission added successfully.",
                        data: result
                    });
                })
                .catch(error => {
                    res.json({
                        success: false,
                        message: "Failed to add offer submission."
                    });
                });
            // res.json({
            //     success: true,
            //     message: "Offer request submitted.",
            //     data: result
            // });
        })
        .catch(error => {
            res.json({
                success: false,
                message: error
            });
        });
};

exports.getSubmissionsForOffer = function (req, res) {
    const userId = req.body.userId;
    console.log("userId", userId)
    OfferSubmission.find({ offerId: req.params.id }).populate("userId", "-password")
        .then(result => {
            res.json({
                success: true,
                data: result,
                message: "my offers fetched"
            });
        })
        .catch(error => {
            res.json({
                success: false,
                message: "Unable to fetch my offers"
            });
        });
}


exports.buyOfferFromPatient = function (req, res) {
    let data = req.body;
    const patientId = req.query.patientId;
    Offer.findByIdAndUpdate(req.params.id, {
        $set: {
            hasPurchased: true,
            purchasedFrom: patientId
        },

    }, { new: true })
        .then(result => {
            res.json({
                success: true,
                message: "Offer purchased."
            });
        })
        .catch(error => {
            res.json({
                success: false,
                message: "Failed to update offer."
            });
        });
};