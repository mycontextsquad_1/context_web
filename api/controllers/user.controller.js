const User = require("../models/user.model");
const RequestAccess = require("../models/request-access.model");
const BuyRecords = require("../models/buy.model");
const Profile = require("../models/profile.model");
const Token = require("../models/token.model");
const crypto = require("crypto");
const nodemailer = require("nodemailer");
const jwt = require("jsonwebtoken");
const Request = require("request");

exports.listUsers = function (req, res) {
  User.find({ user_type: "Patient" }, ["_id", "name", "email"], function (error, users) {
    if (error) {
      var output = {
        success: false,
        message: "Unable to list users."
      };
      res.json(output);
    }

    var output = {
      success: true,
      message: "Successfully fetched users list.",
      data: users
    };
    res.json(output);
  });
};

exports.listAllUsersForAdmin = function (req, res) {
  let q = { user_type: { $ne: "Admin" } }
  if (req.body) {
    let body = req.body;
    if (body.userType && body.userType !== "all") {
      q = { user_type: body.userType }
    }
    if (body.name) {
      q.name = { $regex: ".*" + body.name + ".*", $options: "i" }
    }
  }
  User.find(q, ["_id", "name", "email", "user_type"], function (error, users) {
    if (error) {
      var output = {
        success: false,
        message: "Unable to list users."
      };
      res.json(output);
    }
    var output = {
      success: true,
      message: "Successfully fetched users list.",
      data: users
    };
    res.json(output);
  });
};

exports.getDoctorRequests = function (req, res) {
  RequestAccess.find({ doctorId: req.body.userId }, function (error, users) {
    if (error) {
      var output = {
        success: false,
        message: "Unable to list users."
      };
      res.json(output);
    }
    var output = {
      success: true,
      message: "Successfully fetched users list.",
      data: users
    };
    res.json(output);
  });
};

exports.requestPatientForAccess = function (req, res) {
  let requestAccess = new RequestAccess();
  let data = req.body;
  requestAccess.doctorId = data.userId;
  requestAccess.patientId = data.patientId;
  requestAccess.save()
    .then(result => {
      res.json({
        success: true,
        message: "Requested successfully."
      });
    })
    .catch(error => {
      res.json({
        success: false,
        message: "Failed to request."
      });
    });
}

exports.getPatientRequestByDoctors = function (req, res) {
  RequestAccess.find({ patientId: req.body.userId, hasAccepted: false }).populate("doctorId").then(result => {
    res.json({
      success: true,
      data: result,
      message: "request fetched"
    });
  })
    .catch(error => {
      res.json({
        success: false,
        message: "Unable to fetch request"
      });
    });
}

exports.getDoctorsApprovedPatients = function (req, res) {
  RequestAccess.find({ doctorId: req.body.userId, hasAccepted: true }).populate("patientId").then(result => {
    res.json({
      success: true,
      data: result,
      message: "patients fetched"
    });
  })
    .catch(error => {
      res.json({
        success: false,
        message: "Unable to fetch patients"
      });
    });
}

exports.acceptDoctorRequest = function (req, res) {
  RequestAccess.findByIdAndUpdate(req.body._id, {
    "$set": {
      hasAccepted: true
    }
  }, function (error, users) {
    if (error) {
      var output = {
        success: false,
        message: "Unable to accept request."
      };
      res.json(output);
    }
    var output = {
      success: true,
      message: "Successfully accepted request.",
      data: users
    };
    res.json(output);
  });
}


exports.buyRecord = function (req, res) {
  const data = req.body;
  const newRecord = new BuyRecords();
  newRecord.facilitatorAccessToken = data.facilitatorAccessToken;
  newRecord.orderID = data.orderID;
  newRecord.payerID = data.payerID;
  newRecord.price = data.price;
  newRecord.recordId = data.recordId;
  newRecord.userId = data.userId;
  newRecord
    .save()
    .then(result => {
      return res.json({
        success: true,
        message: "Payment done successfully."
      });
    })
    .catch(error => {
      res.json({
        success: false,
        message: "Payment failed"
      });
    });
};


exports.register = function (req, res) {
  User.findOne(
    {
      email: req.body.email
    },
    function (err, user) {
      if (err) {
        throw err;
      }

      const new_user = new User({
        name: req.body.name,
        email: req.body.email,
        password: req.body.password,
        user_type: req.body.user_type,
        isVerified: false
      });

      if (user) {
        res.json({
          success: false,
          message: "User already exists."
        });
      } else {
        new_user.save(function (error, result) {
          if (error) {
            res.json({
              success: false,
              message: "User registration failed."
            });
          }

          Request.post(
            {
              headers: { "content-type": "application/json" },
              url: "http://137.135.94.61:3000/api/com.mycontext.Owner",
              body: JSON.stringify({
                $class: "com.mycontext.Owner",
                ownerId: new_user._id,
                name: new_user.name
              })
            },
            function (err, httpResponse, body) {
              if (err) {
                result.remove();
                res.json({
                  success: false,
                  message: "User registration failed."
                });
              }

              return sendVerificationMail(result, req, res, req.body.uiUrl)

            }
          );
        });
      }
    }
  );
};

exports.login = function (req, res) {
  User.findOne(
    {
      email: req.body.email
    },
    function (err, user) {
      if (err) {
        throw err;
      }

      if (user) {
        user.comparePassword(req.body.password, function (err, isMatch) {
          if (err) {
            throw err;
          }

          if (isMatch) {
            if (user.isVerified) {
              const payload = {
                userid: user._id
              };

              var token = jwt.sign(payload, "MyContext", {
                expiresIn: 60 * 60 * 60 * 240
              });
              console.log("loggedIn user", user)
              res.json({
                success: true,
                message: "Access token generation successfull.",
                token: token,
                name: user.name,
                email: user.email,
                user_id: user._id,
                user_type: user.user_type
              });
            } else {
              res.json({
                success: false,
                message: "Please verify your account",
                token: token,
                name: user.name,
                email: user.email,
                user_type: user.user_type
              });
            }
          } else {
            res.json({
              success: false,
              message: "Authentication failed. Wrong password."
            });
          }
        });
      } else {
        res.json({
          success: false,
          message: "Authentication failed. User not found."
        });
      }
    }
  );
};

exports.confirmation = function (req, res) {
  Token.findOne({ token: req.params.token, otp: req.query.otp }).then(result => {
    if (!result) {
      return res.status(200).json({
        success: false,
        message:
          "We were unable to find a valid token with OTP or token expired."
      });
    }

    User.findOne({ _id: result._userId })
      .then(user => {
        if (!user) {
          return res.status(200).json({
            success: false,
            message: "We were unable to find a user for this token."
          });
        }

        if (user.isVerified) {
          return res.status(200).json({
            success: false,
            message: "This user has already been verified."
          });
        }

        user.isVerified = true;

        user
          .save()
          .then(userResult => {
            res.json({
              success: true,
              message: "The account has been verified. Please log in",
            });
          })
          .catch(error => {
            return res
              .status(500)
              .json({ success: false, message: error.message });
          });
      })
      .catch(error => {
        return res.status(500).json({ success: false, message: error.message });
      });
  });
};

/* Function to send email */
sendVerificationMail = function (user, req, res, uiUrl) {
  console.log("send mail")
  let OTP = Math.floor(Math.random() * 90000) + 10000;
  var token = new Token({
    _userId: user._id,
    token: crypto.randomBytes(16).toString("hex"),
    otp: OTP
  });
  console.log("token", token)

  token
    .save()
    .then(result => {
      var email = "mycontextdeakin@gmail.com";
      var password = "D0cum3ntum!";
      var transporter = nodemailer.createTransport({
        service: "gmail",
        auth: {
          user: email,
          pass: password
        }
      });

      console.log("transporter")

      var link =
        uiUrl + "/confirm-email/" +
        token.token;

      console.log(link);
      var mailOptions = {
        from: email,
        to: user.email,
        subject: "Context account verification",
        text: "Your OTP is " + OTP + " Click to activate your account " + link
      };

      transporter
        .sendMail(mailOptions)
        .then(result => {
          res.status(200).json({
            success: true,
            message: "A verification email has been sent to " + user.email + "."
          });
        })
        .catch(error => {
          return res
            .status(500)
            .json({ success: false, message: error.message });
        });
    })
    .catch(error => {
      return res.status(500).json({ success: false, message: error.message });
    });
};

exports.getUserProfileDetails = function (req, res) {
  const userId = req.body.userId;
  console.log("userId", userId)
  Profile.findOne({ userId: userId })
    .then(result => {
      res.json({
        success: true,
        data: result,
        message: "User details fetched"
      });
    })
    .catch(error => {
      res.json({
        success: false,
        message: "Unable to fetch user details"
      });
    });
}
exports.getUserWalletDetails = function (req, res) {
  const userId = req.body.userId;
  console.log("userId", userId)
  User.findOne({ _id: userId })
    .then(result => {
      res.json({
        success: true,
        data: result,
        message: "User details fetched"
      });
    })
    .catch(error => {
      res.json({
        success: false,
        message: "Unable to fetch user details"
      });
    });
}
exports.updateUserWalletDetails = function (req, res) {
  const userId = req.body.userId;
  console.log("userId", userId)
  User.findById(userId)
    .then(result => {
      if (!result.walletBalance) {
        result.walletBalance = 0
      }
      result.walletBalance = result.walletBalance + Number(req.body.walletBalance);
      User.findByIdAndUpdate(userId, {
        "$set": {
          walletBalance: result.walletBalance
        }
      }, { new: true })
        .then(userDetails => {
          res.json({
            success: true,
            data: userDetails,
            message: "Wallet updated successfully."
          });
        })
        .catch(error => {
          res.json({
            success: false,
            message: "Failed to update wallet."
          });
        });
    })
    .catch(error => {
      res.json({
        success: false,
        message: "Unable to fetch user details"
      });
    });
}

exports.updateUserProfileDetails = function (req, res) {
  const userId = req.body.userId;
  console.log("userId", userId);
  Profile.findOne({ userId: userId })
    .then(result => {
      if (result) {
        Profile.findByIdAndUpdate(req.body._id, req.body)
          .then(result => {
            res.json({
              success: true,
              message: "Details updated successfully."
            });
          })
          .catch(error => {
            res.json({
              success: false,
              message: "Failed to update details."
            });
          });
      } else {
        let profileDetails = new Profile();
        let data = req.body;
        profileDetails.organization = data.organization;
        profileDetails.designation = data.designation;
        profileDetails.phone = data.phone;
        profileDetails.address = data.address;
        profileDetails.userId = data.userId;
        profileDetails.save()
          .then(result => {
            res.json({
              success: true,
              message: "Details updated successfully."
            });
          })
          .catch(error => {
            res.json({
              success: false,
              message: "Failed to update details."
            });
          });
      }
    })
    .catch(error => {
      res.json({
        success: false,
        message: "Unable to fetch user details"
      });
    });
}