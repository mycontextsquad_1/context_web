import React, { Component } from 'react';
import { ToastsStore, ToastsContainer, ToastsContainerPosition } from 'react-toasts';
import axios from 'axios';
import { API_URL, USER_ROLES } from '../common';
import UnAuthHeader from './UnAuthHeader';
import "../css/SignUp.css"
import CustomLoading from './Loading'
class SignUpPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            register: {
                name: "",
                email: "",
                password: "",
                confirmPassword: "",
                user_type: "Patient",
                loading: false
            }
        }
    }
    handleRegisterChange = (event, value) => {
        let eve = { ...event }
        this.setState(prevState => ({
            register: {
                ...prevState.register,
                [eve.target.name]: value ? value : eve.target.value
            }
        }))
    }

    handleRegister = event => {
        event.preventDefault();
        var self = this;

        if (this.state.register.password !== this.state.register.confirmPassword) {
            ToastsStore.warning("Password doesn't match");
        } else {
            self.setState({ isLoading: true });

            var url = API_URL + "user/register";

            var payload = { ...this.state.register }

            payload.uiUrl = window.location.origin;



            axios
                .post(url, payload)
                .then(function (response) {
                    if (response.data.success) {
                        ToastsStore.success("Registration successful, please verify email to continue..");
                        self.props.history.push("/verify-email");
                    } else {
                        self.setState({ isLoading: false });
                        ToastsStore.error(response.data.message);
                    }
                })
                .catch(function () {
                    self.setState({ isisLoading: false });
                    ToastsStore.error("Something went wrong!");
                });
        }
    };
    render() {
        return (
            <React.Fragment>
                {this.state.isLoading && (
                    <CustomLoading />
                )}
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <UnAuthHeader />
                <div className="row m-0">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <br />
                        <div className="card">
                            <div className="card-body">
                                <form className="form-sign-in" onSubmit={this.handleRegister}>

                                    <table cellPadding="10" className="w-100">
                                        <tbody>
                                            <tr>
                                                <td colSpan="2">
                                                    <h5 className="m-0 text-center">Your Records in MyCONTEXT</h5>
                                                    <p className="mb-0 mt-2 text-center">
                                                        We Store , Analyse,Process Bids.
                                                    </p>
                                                    {/* <h5 className="m-0 text-center">New User? Register with us now!</h5> */}
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    {/* Email */}
                                                    <div className="align-items-center d-flex">
                                                        <div className="sign-up-icon green-bg">
                                                            <i class="far fa-envelope"></i>
                                                        </div>
                                                        <div className="flex-grow-1">
                                                            <input type="email"
                                                                id="signUptEmail"
                                                                className="form-control sign-up-text-field"
                                                                placeholder="Email address"
                                                                name="email"
                                                                value={this.state.register.email}
                                                                onChange={this.handleRegisterChange}
                                                                required />
                                                        </div>
                                                    </div>
                                                </td>

                                            </tr>
                                            <tr>
                                                <td >
                                                    {/* Username */}
                                                    <div className="align-items-center d-flex">
                                                        <div className="sign-up-icon green-bg">
                                                            <i className="far fa-user"></i>
                                                        </div>
                                                        <div className="flex-grow-1">
                                                            <input type="text"
                                                                id="inputName"
                                                                className="form-control sign-up-text-field"
                                                                placeholder="Name"
                                                                name="name"
                                                                value={this.state.register.name}
                                                                onChange={this.handleRegisterChange}
                                                                required
                                                            />
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    {/* Password */}
                                                    <div className="align-items-center d-flex">
                                                        <div className="sign-up-icon green-bg">
                                                            <i class="fas fa-key"></i>
                                                        </div>
                                                        <div className="flex-grow-1">
                                                            <input type="password"
                                                                id="signupPassword"
                                                                className="form-control sign-up-text-field"
                                                                placeholder="Password"
                                                                name="password"
                                                                value={this.state.register.password}
                                                                onChange={this.handleRegisterChange}
                                                                required />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >
                                                    {/* Confirm Password */}
                                                    <div className="align-items-center d-flex">
                                                        <div className="sign-up-icon green-bg">
                                                            <i class="fas fa-key"></i>
                                                        </div>
                                                        <div className="flex-grow-1">
                                                            <input type="password"
                                                                id="inputConfirmPassword"
                                                                className="form-control sign-up-text-field"
                                                                placeholder="Confirm Password"
                                                                name="confirmPassword"
                                                                value={this.state.register.confirmPassword}
                                                                onChange={this.handleRegisterChange}
                                                                required />
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            {/* <tr >
                                                <td>
                                                    Select User Type:
                                                </td>
                                            </tr> */}
                                            <tr>
                                                <td >
                                                    <div className="align-items-center d-flex">
                                                        <div className="sign-up-icon green-bg">
                                                            <i class="fas fa-user-tag"></i>
                                                        </div>
                                                        <div className="flex-grow-1">
                                                            <select className="form-control sign-up-text-field" name="user_type" onChange={this.handleRegisterChange}>
                                                                {USER_ROLES.map(role => (
                                                                    <option value={role} key={role}>{role}</option>
                                                                ))}

                                                                {/* <option value="Patient"> Patient</option>
                                                                <option value="Doctor"> Doctor</option>
                                                                <option value="Hospital"> Hospital</option>
                                                                <option value="Pathologist"> Pathologist</option>
                                                                <option value="Pharmaceutical Company"> Pharmaceutical Company</option>
                                                                <option value="Pathology Laboratory"> Pathology Laboratory</option>
                                                                <option value="Insurance Company"> Insurance Company</option> */}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    {/* <div className="mt--10"> */}
                                                    {/* <label className="mr-2" ><input name="user_type" type="radio" value="Patient" onChange={this.handleRegisterChange} checked={this.state.register.user_type === "Patient"} /> Patient</label>
                                                        <label className="mr-2"><input name="user_type" type="radio" value="Doctor" onChange={this.handleRegisterChange} checked={this.state.register.user_type === "Doctor"} /> Doctor</label>
                                                        <label className="mr-2"><input name="user_type" type="radio" value="Hospital" onChange={this.handleRegisterChange} checked={this.state.register.user_type === "Hospital"} /> Hospital</label>
                                                        <label className="mr-2"><input name="user_type" type="radio" value="Pathologist" onChange={this.handleRegisterChange} checked={this.state.register.user_type === "Pathologist"} /> Pathologist</label>
                                                        <label className="mr-2"><input name="user_type" type="radio" value="Pharmaceutical Company" onChange={this.handleRegisterChange} checked={this.state.register.user_type === "Pharmaceutical Company"} /> Pharmaceutical Company</label>
                                                        <label className="mr-2"><input name="user_type" type="radio" value="Pathology Laboratory" onChange={this.handleRegisterChange} checked={this.state.register.user_type === "Pathology Laboratory"} /> Pathology Laboratory</label>
                                                        <label className="mr-2"><input name="user_type" type="radio" value="Insurance Company" onChange={this.handleRegisterChange} checked={this.state.register.user_type === "Insurance Company"} /> Insurance Company</label> */}
                                                    {/* </div> */}
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    <div className="text-center">
                                                        <button className="btn btn-primary signup-btn">Sign Up</button>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </form>
                            </div>
                        </div>

                        <br />
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default SignUpPage;