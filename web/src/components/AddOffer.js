import React, { Component } from "react";
import NavBar from "./NavBar";
import {
    ToastsContainer,
    ToastsStore,
    ToastsContainerPosition
} from "react-toasts";

import { httpClient } from '../UtilService'
import CustomLoading from "./Loading";
import { CANCER_TYPES, AGE_GROUPS } from "../common";
import OfferRequestModal from "./OfferRequestModal";
import CustomModal from './common/CustomModal'

/*   Make an offer*/

class AddOffer extends Component {
    userId = ""
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            data: {
                cancerType: "Mouth",
                description: undefined,
                tumorSize: 1,
                ageGroup: "all",
                gender: "Male",
                price: undefined
            }
        };
    }

    onClose = () => {
        this.props.onClose()
    }

    addOffer = async (event) => {
        event.preventDefault();
        if (this.state.description !== undefined && this.state.description === "") {
            ToastsStore.error("Description can't be empty");
        } else {
            this.setState({ loading: true });
            console.log(this.state.data);
            var payload = {
                ...this.state.data
            };
            let result = await httpClient("offer/add-offer", "PUT", payload);
            if (result.success) {
                ToastsStore.success("Offer added successfully.");
                console.log(this.state.data);
                this.props.onClose(true)
            } else {
                this.setState({
                    loading: false
                })
                ToastsStore.error(result.message);
            }
        };

    }

    handleChange = event => {
        const dataCopy = this.state.data;
        dataCopy[event.target.name] = event.target.value;
        this.setState({ data: dataCopy });
    };


    render() {
        return (
            <React.Fragment>
                <CustomModal size="lg" title="Add Offer" onClose={() => this.onClose()} >
                    <ToastsContainer
                        store={ToastsStore}
                        position={ToastsContainerPosition.TOP_RIGHT}
                    />
                    {this.state.loading && (
                        <CustomLoading />
                    )}
                    <form onSubmit={this.addOffer}>
                        <div className="row m-0">
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label htmlFor="inputPrice">Price</label>
                                    <input
                                        type="number"
                                        id="inputPrice"
                                        className="form-control"
                                        placeholder="Price"
                                        name="price"
                                        value={this.state.data.price}
                                        onChange={this.handleChange}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group mb-0">
                                    <label>Cancer Type</label>
                                    <select required className="form-control" name="cancerType" value={this.state.data.cancerType} onChange={this.handleChange}>
                                        {CANCER_TYPES.map(x => (
                                            <option value={x.label} key={x.label}> {x.value}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label>Gender</label>
                                    <select className="form-control" name="gender" value={this.state.data.gender} onChange={this.handleChange}>
                                        <option value="Male"> Male</option>
                                        <option value="Female"> Female</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label>Tumor Size</label>
                                    <select required className="form-control" name="tumorSize" value={this.state.data.tumorSize} onChange={this.handleChange}>
                                        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(x => (
                                            <option value={x} key={x}> {x}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label htmlFor="start_date">Start Date</label>
                                    <input
                                        type="date"
                                        id="startDate"
                                        className="form-control"
                                        placeholder="Start Date"
                                        name="startDate"
                                        value={this.state.data.startDate}
                                        onChange={this.handleChange}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label htmlFor="end_date">End Date</label>
                                    <input
                                        type="date"
                                        id="endDate"
                                        className="form-control"
                                        placeholder="End Date"
                                        name="endDate"
                                        value={this.state.data.endDate}
                                        onChange={this.handleChange}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label htmlFor="outcome">Description</label>
                                    <input
                                        type="text"
                                        id="Description"
                                        className="form-control"
                                        placeholder="Description"
                                        name="description"
                                        value={this.state.data.description}
                                        onChange={this.handleChange}
                                        required
                                    />
                                </div>
                            </div>
                            <div className="col-md-6 col-12">
                                <div className="form-group">
                                    <label>Age(Years)</label>
                                    <select className="form-control" name="ageGroup" value={this.state.data.ageGroup} onChange={this.handleChange}>
                                        <option value="all">All</option>
                                        {AGE_GROUPS.map(x => (
                                            <option value={x} key={x}> {x}</option>
                                        ))}
                                    </select>
                                </div>
                            </div>
                            <div className="col-12 text-right">
                                <br />
                                <button
                                    className="btn btn-sm btn-primary"
                                    type="submit"
                                >
                                    Create
                      </button>
                            </div>
                        </div>
                    </form>
                </CustomModal>
            </React.Fragment >
        );
    }
}

export default AddOffer;