import React, { Component } from 'react';
import { ToastsStore, ToastsContainer, ToastsContainerPosition } from 'react-toasts';
import axios from 'axios';
import { API_URL } from '../common';
import { withRouter } from 'react-router-dom';
import CustomLoading from './Loading'

class UnAuthHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            login: {
                email: "",
                password: "",
            },
        }
    }
    handleLoginChange = (event, value) => {
        let eve = { ...event }
        this.setState(prevState => ({
            login: {
                ...prevState.login,
                [eve.target.name]: value ? value : eve.target.value
            }
        }))
    }
    handleSignIn = event => {
        event.preventDefault();
        var self = this;
        self.setState({ isLoading: true });

        var url = API_URL + "user/login";

        var payload = { ...this.state.login }

        axios
            .post(url, payload)
            .then(function (response) {
                if (response.data.success) {
                    localStorage.setItem("access-token", response.data.token);
                    localStorage.setItem("name", response.data.name);
                    localStorage.setItem("user_id", response.data.user_id);
                    localStorage.setItem("email", response.data.email);
                    localStorage.setItem("user_type", response.data.user_type);
                    if (response.data.user_type === "Patient") {
                        self.props.history.push("/patient-offers");
                    } else {
                        self.props.history.push("/");
                    }
                } else {
                    self.setState({ isLoading: false });
                    ToastsStore.error(response.data.message);
                }
            })
            .catch(function (error) {
                self.setState({ isLoading: false });
                ToastsStore.error("Something went wrong!");
            });
    };
    render() {
        return (
            <React.Fragment>
                {this.state.isLoading && (
                    <CustomLoading />
                )}
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <section className="header">
                    <div className="row">
                        <div className="col d-flex align-items-center">
                            {/* <h2 className="green">
                                My Context
        </h2> */}
                            <img src="/images/logo.png" alt="logo" />
                        </div>
                        <div className="col">
                            <form className="form-signin" onSubmit={this.handleSignIn}>

                                <table align="right" className="mr-4" cellPadding="4">
                                    <thead className="no-custom-bg">
                                        <tr>

                                            <td>
                                                Username
                </td>
                                            <td>
                                                Password
                </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input type="email"
                                                    id="inputEmail"
                                                    className="form-control"
                                                    placeholder="Email address"
                                                    name="email"
                                                    value={this.state.login.email}
                                                    onChange={this.handleLoginChange}
                                                    required
                                                    autoFocus />
                                            </td>
                                            <td>
                                                <input type="password"
                                                    id="inputPassword"
                                                    className="form-control"
                                                    placeholder="Password"
                                                    name="password"
                                                    value={this.state.login.password}
                                                    onChange={this.handleLoginChange}
                                                    required />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colSpan="2" className="text-right">
                                                <button className="btn btn-primary">
                                                    Sign In
                        </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </form>
                        </div>
                    </div>
                </section>
            </React.Fragment>
        );
    }
}

export default withRouter(UnAuthHeader);