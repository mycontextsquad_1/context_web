import React, { Component } from 'react';
import "./newLoginPage.css"
import { ToastsStore, ToastsContainer, ToastsContainerPosition } from 'react-toasts';
import UnAuthHeader from './UnAuthHeader';
import CustomLoading from './Loading'
class NewLoginPage extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {
        document.title = "My Context";
    }

    handleSignUpRedirect = () => {
        this.props.history.push("/signup");
    }


    render() {
        return (
            <React.Fragment>
                {this.state.isLoading && (
                    <CustomLoading />
                )}
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <UnAuthHeader />
                <main role="main" className="landing-main-content">

                    <div className="row m-0 h-100">
                        <div className="align-items-center col d-flex flex-column justify-content-center">
                            <div className="w-100 d-flex justify-content-around mb-3">
                                <i class="fas green fs-40 fa-gavel"></i>
                                <i class="fas green fs-40 fa-file"></i>
                                <i class="fas green fs-40 fa-dollar-sign"></i>
                                <i class="fas green fs-40 fa-shield-alt"></i>
                            </div>
                            <div className="project-content">

                                Your Records in MyCONTEXT
                                Allows users to manage their records. Patient can manage their records by trading and participating in the bidding with the third part users like medical researchers or the pharmaceutical Companies.
                                We assist  patients in updating their medical records with the help of Doctors.

                            </div>
                            <div className="mt-3 text-center">
                                <button onClick={this.handleSignUpRedirect} className="btn btn-primary signup-btn">
                                    Sign Up
                                </button>
                            </div>
                        </div>
                        <div className="col pr-0">
                            {/* <div className="text-center">
                                <i className="fas fa-circle fa-10x green"></i>
                            </div> */}
                            <div className="d-flex flex-column h-100 justify-content-center">
                                <img src="/images/landing.jpg" className="landing-img" alt="landing" />
                                {/* <div>
                                    <i className="fas fa-stethoscope fa-7x mt-4"></i>
                                </div>
                                <br />
                                <br />
                                <div className="d-flex justify-content-around">
                                    <i className="fas fa-chart-pie fa-7x mt-4 text-left"></i>

                                    <i className="fas fa-file-alt fa-7x mt-4 text-right"></i>
                                </div> */}
                            </div>
                        </div>

                    </div>

                </main>
                <footer>
                    <p className="m-0 p-2 text-center">Copyright &copy; {new Date().getFullYear()} All Rights Reserved</p>
                </footer>
            </React.Fragment>
        );
    }
}

export default NewLoginPage;