import React, { Component } from "react";
import NavBar from "./NavBar";
import {
    ToastsContainer,
    ToastsStore,
    ToastsContainerPosition
} from "react-toasts";

import { httpClient } from '../UtilService'
import CustomLoading from "./Loading";

class AllUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true,
            search: {
                userType: "all",
                name: ""
            }
        };
    }

    componentDidMount() {
        document.title = "Users";

        this.getUsers();
    }

    getUsers = async () => {
        var self = this;

        let result = await httpClient("user/admin-users", "POST", this.state.search);
        console.log(result);

        if (result.success) {
            self.setState({ data: result.data, loading: false });
            console.log(self.state.data);
        } else {
            this.setState({
                loading: false
            })
            ToastsStore.error(result.message);
        }
    }

    handleChange = (event, value) => {
        let eve = { ...event }
        this.setState(prevState => ({
            search: {
                ...prevState.search,
                [eve.target.name]: value ? value : eve.target.value
            }
        }))
    }

    render() {
        return (
            <React.Fragment>
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <NavBar {...this.props} />
                <div className="container-fluid">
                    <div className="row">
                        <div
                            role="main"
                            className="col-12 main-container pt-60 mobile-space"
                        >
                            <h2>Users</h2>
                            <hr />
                            <div className="row m-0 align-items-end mb-3">
                                <div className="form-group mb-0 mr-2">
                                    <label>Name</label>
                                    <input type="text"
                                        id="name"
                                        className="form-control"
                                        placeholder="Name"
                                        name="name"
                                        value={this.state.search.name}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group mb-0 mr-2">
                                    <label>User Type</label>
                                    <select className="form-control" name="userType" value={this.state.search.userType} onChange={this.handleChange}>
                                        <option value="all">All</option>
                                        <option value="Patient"> Patient</option>
                                        <option value="Doctor"> Doctor</option>
                                        <option value="Hospital"> Hospital</option>
                                        <option value="Pathologist"> Pathologist</option>
                                        <option value="Pharmaceutical Company"> Pharmaceutical Company</option>
                                        <option value="Pathology Laboratory"> Pathology Laboratory</option>
                                        <option value="Insurance Company"> Insurance Company</option>
                                    </select>
                                </div>
                                <div>
                                    <button className="btn btn-primary" onClick={this.getUsers}>Search</button>
                                </div>
                            </div>
                            {this.state.loading && (
                                <CustomLoading />
                            )}
                            <div className="table-responsive bg-white">
                                <table className="table table-bordered">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Type</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.data.map((record, index) => (
                                            <tr key={index}>
                                                <td>{record.name}</td>
                                                <td>{record.email}</td>
                                                <td>{record.user_type}</td>
                                            </tr>
                                        ))}
                                        {this.state.data.length === 0 && (
                                            <tr>
                                                <td className="text-center" colSpan="3">No Users found</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default AllUsers;
