import React from 'react';
const MyOffersList = (props) => {
    return (
        <React.Fragment>
            <div className="table-responsive bg-white padding-16">
                <table className="table table-bordered">
                    <thead className="thead-light">
                        <tr>
                            <th scope="col">Description</th>
                            <th scope="col">Cancer Type</th>
                            <th scope="col">Age Group(Years)</th>
                            <th scope="col">Price</th>
                            <th scope="col">Tumor Size</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Period</th>
                            {props.status === 'pending' && (
                                <th scope="col">Action</th>
                            )}
                            {props.status === 'purchased' && (
                                <th scope="col">Purchased From</th>
                            )}
                        </tr>
                    </thead>
                    <tbody>
                        {props.data.map((offer, index) => (
                            <tr key={index}>
                                <td>{offer.description}</td>
                                <td>{offer.cancerType}</td>
                                <td>{offer.ageGroup}</td>
                                <td>{offer.price}</td>
                                <td>{offer.tumorSize}</td>
                                <td>{offer.gender}</td>
                                <td>{new Date(offer.startDate).toDateString()} - {new Date(offer.endDate).toDateString()}</td>
                                {props.status === 'pending' && (
                                    <td>
                                        <button onClick={() => props.handleApproveModal(offer)} type="button" className="btn btn-primary">
                                            {offer.noOfUsersSubmitted} Requests
                                                            </button>
                                    </td>
                                )}
                                {props.status === 'purchased' && (
                                    <td>
                                        {offer.hasPurchased && (
                                            <React.Fragment>{offer.purchasedFrom.name}({offer.purchasedFrom.email})</React.Fragment>
                                        )}
                                    </td>

                                )}
                            </tr>
                        ))}
                        {props.data.length === 0 && (
                            <tr>
                                <td className="text-center" colSpan="8">No Offers found</td>
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        </React.Fragment>
    );
}

export default MyOffersList;