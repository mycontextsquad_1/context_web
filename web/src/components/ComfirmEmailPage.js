import React, { Component } from 'react';
import { ToastsStore, ToastsContainer, ToastsContainerPosition } from 'react-toasts';
import { httpClient } from '../UtilService';
import CustomLoading from './Loading'
class ConfirmEmailPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            message: "",
            otp: "",
            isEmailVerified: false
        }
    }
    componentDidMount() {
        // this.verifyAccount();
    }
    verifyAccount = async () => {
        let token = window.location.href.split("/").pop()
        let url = "user/confirmation/" + token;
        let result = await httpClient(url, "GET", { otp: this.state.otp });
        console.log(result);
        let self = this;
        if (result.success) {
            ToastsStore.success("Verification successful, login to continue..");
            this.setState({
                isEmailVerified: true
            })
            // setTimeout(() => {
            //     self.props.history.push("/login");
            // }, 2000);
        } else {
            self.setState({ isLoading: false, message: result.message });
            ToastsStore.error(result.message);
        }
    }
    handleChange = event => {
        let eve = { ...event };
        this.setState({
            [eve.target.name]: eve.target.value
        });
    };
    verifyOTP = () => {
        if (!this.state.otp) {
            ToastsStore.error("Please enter OTP");
        } else {
            this.verifyAccount()
        }
    }
    navigateToLogin = () => {
        this.props.history.push("/login");
    }
    render() {
        return (
            <section className="padding-16">
                {this.state.isLoading && (
                    <CustomLoading />
                )}
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-4 mx-auto mtb">
                        <br />
                        <br />
                        <div className="card card-signin">
                            <div className="card-body">
                                {this.state.isEmailVerified ? (
                                    <div >
                                        <img src="/images/email-verified.jpg" className="mb-4 w-100" alt="email" />
                                        <h5 className="m-0 text-center">
                                            Your Email has been verified. Please click on login to continue
                                        </h5>
                                        <div className="text-center mt-5">
                                            <button className="btn btn-primary signup-btn" onClick={this.navigateToLogin}>Login</button>
                                        </div>
                                    </div>
                                ) : (
                                        <form>
                                            <h5 className="text-center">Verify</h5>
                                            <div className="form-group">
                                                <label htmlFor="designation">OTP</label>
                                                <input
                                                    type="number"
                                                    id="otp"
                                                    className="form-control"
                                                    placeholder="OTP"
                                                    name="otp"
                                                    value={this.state.otp}
                                                    onChange={this.handleChange}
                                                    autoFocus
                                                    required
                                                />
                                                <div className="text-center mt-3">
                                                    <button type="button" className="btn btn-primary" onClick={this.verifyOTP}>Verify</button>
                                                </div>
                                            </div>
                                        </form>
                                    )}

                            </div>
                        </div>
                    </div>
                </div>

            </section>
        );
    }
}

export default ConfirmEmailPage;