import React, { Component } from "react";
import NavBar from "./NavBar";
import {
    ToastsContainer,
    ToastsStore,
    ToastsContainerPosition
} from "react-toasts";

import { httpClient } from '../UtilService'
import CustomLoading from "./Loading";
import OfferApproveModal from "./OfferApproveModal";
import { Nav, NavItem, TabContent, TabPane, NavLink } from 'reactstrap'
import MyOffersList from "./MyOffersList";
import AddOffer from "./AddOffer";


class Offers extends Component {
    userId = ""
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true,
            status: "new",
            offerApproveModal: {
                isOpen: false,
            },
            addOfferDialog: {
                isOpen: false,
            }
        };
    }

    componentDidMount() {
        document.title = "Offers";
        this.userId = localStorage.getItem("user_id")
        this.getOffers();
    }

    getOffers = async () => {
        var self = this;
        this.setState({
            loading: true
        })
        let result = await httpClient("offer/my-offers", "GET", { status: this.state.status });
        console.log(result);
        if (result.success) {
            self.setState({ data: result.data, loading: false });
            console.log(self.state.data);
        } else {
            this.setState({
                loading: false
            })
            ToastsStore.error(result.message);
        }
    }

    handleStatusChange = (status) => {
        this.setState({
            status: status
        }, this.getOffers)
    }

    handleApproveModal = (offer) => {
        this.setState({
            offerApproveModal: {
                isOpen: true,
                data: offer
            }
        });
    }

    handleClose = async (data) => {
        if (data) {
            this.setState({
                offerApproveModal: {
                    isOpen: false,
                    data: undefined
                },
                loading: false
            }, this.getOffers)

        } else {
            this.setState({
                offerApproveModal: {
                    isOpen: false,
                    data: undefined
                }
            })
        }
    }

    handleAddOfferDialog = () => {
        this.setState({
            addOfferDialog: {
                isOpen: true,
            }
        });
    }

    handleCloseOfferDialog = (data) => {
        this.setState({
            addOfferDialog: {
                isOpen: false,
            }
        });
    }

    render() {
        return (
            <React.Fragment>
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <NavBar {...this.props} />
                <div className="container-fluid">
                    <div className="row">
                        <div
                            role="main"
                            className="col-12 main-container pt-60 mobile-space"
                        >
                            <h2>My Offers</h2>
                            <hr />
                            {this.state.loading && (
                                <CustomLoading />
                            )}
                            <Nav tabs>
                                <NavItem>
                                    <NavLink
                                        className={`${this.state.status === 'new' ? 'active' : ''}`}
                                        onClick={() => { this.handleStatusChange('new'); }}
                                    >
                                        New
                                     </NavLink>
                                </NavItem>
                                <NavItem>

                                    <NavLink
                                        className={`${this.state.status === 'pending' ? 'active' : ''}`}
                                        onClick={() => { this.handleStatusChange('pending'); }}
                                    >
                                        Pending
                                     </NavLink>
                                </NavItem>
                                <NavItem>

                                    <NavLink
                                        className={`${this.state.status === 'purchased' ? 'active' : ''}`}
                                        onClick={() => { this.handleStatusChange('purchased'); }}
                                    >
                                        Purchased
                                     </NavLink>
                                </NavItem>
                            </Nav>
                            <TabContent activeTab={this.state.status}>
                                <TabPane tabId="new">
                                    <div className="row justify-content-end m-0 padding-16">
                                        <button className="btn btn-primary btn-sm" onClick={() => this.handleAddOfferDialog()}>
                                            Add Offer                  </button>
                                    </div>
                                    <MyOffersList handleApproveModal={this.handleApproveModal} status={this.state.status} data={this.state.data} />
                                </TabPane>
                                <TabPane tabId="pending">
                                    <MyOffersList handleApproveModal={this.handleApproveModal} status={this.state.status} data={this.state.data} />
                                </TabPane>
                                <TabPane tabId="purchased">
                                    <MyOffersList handleApproveModal={this.handleApproveModal} status={this.state.status} data={this.state.data} />
                                </TabPane>
                            </TabContent>

                        </div>
                    </div>
                </div>
                {
                    this.state.offerApproveModal.isOpen && (
                        <React.Fragment>
                            <OfferApproveModal selectedOffer={this.state.offerApproveModal.data} onClose={this.handleClose} />
                        </React.Fragment>
                    )
                }

                {this.state.addOfferDialog.isOpen && (
                    <React.Fragment>
                        <AddOffer onClose={this.handleCloseOfferDialog} />
                        {/* <PaymentDialog onClose={this.handlePaymentDialogClose} /> */}
                    </React.Fragment>
                )}
            </React.Fragment >
        );
    }
}

export default Offers;
