import React from 'react';
const CustomLoading = (props) => {
    return (
        <React.Fragment>
            {/* <div className="loading"> */}
            {/* <CustomModal className="loading modal-dialog-centered"> */}
            <div className="loading text-center">
                <img src="/images/loading.gif" className="loading-img" alt="loading" />
                <p className="m-0">Please wait...</p>
            </div>
        </React.Fragment>
    );
}

export default CustomLoading;