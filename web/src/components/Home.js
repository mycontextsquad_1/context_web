import React, { Component } from "react";
import "../css/Home.css";
import NavBar from "./NavBar";
import axios from "axios";
import {
  ToastsContainer,
  ToastsStore,
  ToastsContainerPosition
} from "react-toasts";

import { API_URL, CANCER_TYPES } from "../common";
import PaymentModal from './PaymentModal'
import { httpClient } from '../UtilService'
import CustomLoading from "./Loading";
import AddRecordDialog from "./AddRecordDialog";
import { withRouter } from "react-router-dom";

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_type: localStorage.getItem("user_type"),
      data: [],
      loading: true,
      paymentDialog: {
        isOpen: false,
        selectedRecord: undefined,
        totalRecordsBought: 0
      },
      search: {
        cancerType: "all",
        tumorSize: "all",
        ageGroup: "all",
        gender: "all"
      },
      addRecordDialog: {
        isOpen: false
      },
      walletBalance: 0,
    };
  }

  componentDidMount() {
    if(this.state.user_type==="Patient"){this.props.history.push('/patient-offers')}
    document.title = "Home";
    document.body.classList.add("white");

    this.listRecords()
    this.getUserWalletDetails()
  }

  getUserWalletDetails = async () => {
    let result = await httpClient("user/wallet-details", "GET");
    if (result && result.success && result.data) {
      this.setState(prevState => ({
        walletBalance: result.data.walletBalance,
      }))
    }
  }

  handleChange = (event, value) => {
    let eve = { ...event }
    this.setState(prevState => ({
      search: {
        ...prevState.search,
        [eve.target.name]: value ? value : eve.target.value
      }
    }))
  }

  listRecords = () => {
    var self = this;

    var url = API_URL + "record/listRecords";

    let search = { ...this.state.search };
    if (search.cancerType === "all") {
      delete search.cancerType;
    }
    if (search.tumorSize === "all") {
      delete search.tumorSize;
    }
    if (search.ageGroup === "all") {
      delete search.ageGroup;
    }
    if (search.gender === "all") {
      delete search.gender;
    }

    var payload = {
      token: localStorage.getItem("access-token"),
      from: 0,
      size: 100,
      ...search
    };

    axios
      .post(url, payload)
      .then(function (response) {
        if (response.data.success) {
          self.setState({ data: response.data.data, loading: false });
          console.log(self.state.data);
        } else {
          ToastsStore.error(response.data.message);
        }
      })
      .catch(function () {
        ToastsStore.error("Something went wrong!");
      });
  }

  componentWillUnmount() {
    document.body.classList.remove("white");
  }

  handlePaymentDialogClose = async (data) => {
    // $("#paymentDialog").modal("hide");
    if (data) {
      let self = this;
      self.setState({ loading: true });

      // var url = API_URL + "user/buy-record";
      data.userId = localStorage.getItem("user_id");
      data.recordId = self.state.paymentDialog.selectedRecord._id
      var result = await httpClient("user/buy-record", "POST", data);
      console.log("payment ", result);
      if (result.success) {
        self.setState({
          paymentDialog: {
            isOpen: false,
            selectedRecord: undefined
          },
          loading: false
        })
        ToastsStore.success("Payment Done.");
      } else {
        self.setState({ loading: false });
        ToastsStore.error(result.message);
      }
    } else {
      this.setState({
        paymentDialog: {
          isOpen: false,
          selectedRecordId: undefined
        }
      })
    }
  }

  handlePaymentDialogOpen = async (record) => {
    this.setState({
      isLoading: true
    });
    let result = await httpClient(`record/my-records?userId=${localStorage.getItem("user_id")}`, "GET");
    let totalRecordsBought = 0;
    if (result && result.success) {
      totalRecordsBought = result.data.length;
    }
    this.setState({
      paymentDialog: {
        isOpen: true,
        selectedRecord: record,
        totalRecordsBought
      }
    });
  }

  handleAddRecordDialog = () => {
    this.setState({
      addRecordDialog: {
        isOpen: true,
      }
    });
  }

  handleCloseRecordDialog = (data) => {
    if (data) {
      this.listRecords()
    }
    this.setState({
      addRecordDialog: {
        isOpen: false,
      }
    });
  }

  buyWithWalletBalance = async () => {
    this.setState({
      isLoading: true
    });
    let result = await httpClient("user/wallet-details", "PATCH", { walletBalance: this.state.paymentDialog.selectedRecord.price * -1 });
    if (result && result.success) {
      let time = new Date().getTime();
      let paymentDetails = {
        facilitatorAccessToken: "wallet-ballance",
        orderID: time,
        payerID: time,
        price: this.state.paymentDialog.selectedRecord.price
      }
      this.handlePaymentDialogClose(paymentDetails);
      this.getUserWalletDetails();
    } else {
      ToastsStore.error("Error while updating.");
    }
  }

  render() {
    return (
      <React.Fragment>
        <ToastsContainer
          store={ToastsStore}
          position={ToastsContainerPosition.TOP_RIGHT}
        />
        <NavBar {...this.props} />
        <div className="container-fluid">
          <div className="row">
            {/* <nav className="col-md-2 d-none d-md-block bg-light sidebar">
              <SideBar current="home" />
            </nav> */}
            <div
              role="main"
              className="col-12 main-container pt-60 mobile-space"
            >
              <div className="align-items-center m-0 row justify-content-between">
                <h2>Medical Records</h2>
                {["Doctor", "Hospital"].indexOf(this.state.user_type) > -1 && (
                  <button className="btn btn-primary btn-sm" onClick={() => this.handleAddRecordDialog()}>
                    Add Record
                  </button>
                )}
              </div>
              <hr />
              <div className="row m-0 align-items-end mb-3">
                <div className="form-group mb-0 mr-2">
                  <label>Cancer Type</label>
                  <select className="form-control" name="cancerType" value={this.state.search.cancerType} onChange={this.handleChange}>
                    <option value="all">All</option>
                    {CANCER_TYPES.map(x => (
                      <option value={x.label} key={x.label}> {x.value}</option>
                    ))}
                  </select>
                </div>
                <div className="form-group mb-0 mr-2">
                  <label>Tumor Size</label>
                  <select className="form-control" name="tumorSize" value={this.state.search.tumorSize} onChange={this.handleChange}>
                    <option value="all">All</option>
                    {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(x => (
                      <option value={x} key={x}> {x}</option>
                    ))}
                  </select>
                </div>
                <div className="form-group mb-0 mr-2">
                  <label>Gender</label>
                  <select className="form-control" name="gender" value={this.state.search.gender} onChange={this.handleChange}>
                    <option value="all">All</option>
                    <option value="Male"> Male</option>
                    <option value="Female"> Female</option>
                  </select>
                </div>
                <div>
                  <button className="btn btn-primary" onClick={this.listRecords}>Search</button>
                </div>
              </div>

              {this.state.loading && (
                <CustomLoading />
              )}
              <div className="table-responsive bg-white">
                <table className="table table-bordered">
                  <thead className="thead-light">
                    <tr>
                      {/* <th scope="col">Name</th> */}
                      <th scope="col">Tumor Size</th>
                      <th scope="col">Gender</th>
                      <th scope="col">Cancer Type</th>
                      <th scope="col">Year of Birth</th>
                      <th scope="col">Price</th>
                      <th scope="col">Bid</th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.data.map((record, index) => (
                      <tr key={index}>
                        {/* <th scope="row">{record.name}</th> */}
                        <td>{record.cs_tumor_size}</td>
                        <td>{record.gender}</td>
                        <td>{record.cancer_type}</td>
                        <td>{record.year_of_birth}</td>
                        <td>{record.price}</td>
                        <td>
                          <button onClick={() => this.handlePaymentDialogOpen(record)} type="button" className="btn btn-primary">
                            Buy
                          </button>
                        </td>
                      </tr>
                    ))}
                    {this.state.data.length === 0 && (
                      <tr>
                        <td colSpan="6">
                          <p className="m-0 text-center"> No data found</p>
                        </td>
                      </tr>
                    )}
                    <tr>

                    </tr>
                  </tbody>
                </table>

                {this.state.paymentDialog.isOpen && (
                  <React.Fragment>
                    <PaymentModal totalRecordsBought={this.state.paymentDialog.totalRecordsBought} walletBalance={this.state.walletBalance} buyWithWalletBalance={this.buyWithWalletBalance} selectedRecord={this.state.paymentDialog.selectedRecord} onClose={this.handlePaymentDialogClose} />
                    {/* <PaymentDialog onClose={this.handlePaymentDialogClose} /> */}
                  </React.Fragment>
                )}
              </div>
            </div>
          </div>
        </div>

        {this.state.addRecordDialog.isOpen && (
          <React.Fragment>
            <AddRecordDialog onClose={this.handleCloseRecordDialog} />
          </React.Fragment>
        )}
      </React.Fragment>
    );
  }
}

export default withRouter(Home);
