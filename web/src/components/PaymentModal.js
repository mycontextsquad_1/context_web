
/* Payment module*/
import React, { Component } from 'react';
import CustomModal from './common/CustomModal';
import { PayPalButton } from "react-paypal-button-v2";
class PaymentModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedRecord: { ...props.selectedRecord },
            recordPrice: this.props.selectedRecord.price,
            cutOffPrice: 0
        }
    }
    componentDidMount() {
        let recordPrice = Number(this.props.selectedRecord.price);
        let cutOffPrice = 0
        if (recordPrice > 20) {
            if (this.props.totalRecordsBought >= 5) {
                cutOffPrice = Math.floor(this.props.totalRecordsBought / 5)
                recordPrice = recordPrice - cutOffPrice
                if (recordPrice <= 5) {
                    recordPrice = 5;
                    cutOffPrice = this.state.recordPrice - 5
                }

            }
        }
        this.setState({
            recordPrice,
            cutOffPrice
        })
    }
    onClose = () => {
        this.props.onClose()
    }
    render() {
        return (
            <React.Fragment>
                <CustomModal title="Payment Dialog" onClose={() => this.onClose()} >
                    <div>
                        {this.state.cutOffPrice > 0 ? (
                            <React.Fragment>

                                <div className="d-flex justify-content-between">
                                    <p>Record Price  </p>
                                    <p>${this.state.selectedRecord.price}</p>
                                </div>
                                <div className="d-flex justify-content-between">
                                    <p>For Frequently buying </p>
                                    <p>-${this.state.cutOffPrice}</p>
                                </div>
                                <hr />
                                <div className="d-flex justify-content-between">
                                    <h6>Final Record Price  </h6>
                                    <h6>${this.state.recordPrice}</h6>
                                </div>
                                <hr />
                            </React.Fragment>
                        ) : (
                                <React.Fragment>
                                    {this.props.totalRecordsBought != undefined && (
                                        <div className="d-flex justify-content-between">
                                            <h6>Record Price  </h6>
                                            <h6>${this.state.recordPrice}</h6>
                                        </div>
                                    )}
                                </React.Fragment>
                            )}
                    </div>

                    {this.props.walletBalance && (
                        <React.Fragment>
                            <br />
                            <br />
                            <h5>Your wallet balance -  ${this.props.walletBalance}</h5>
                            <br />
                            {this.props.walletBalance >= Number(this.state.selectedRecord.price) && (
                                <button onClick={() => this.props.buyWithWalletBalance()} type="button" className="btn btn-primary btn-block mb-3">
                                    Buy with Wallet Balance
                                </button>
                            )}
                        </React.Fragment>
                    )}
                    <PayPalButton
                        amount={this.state.recordPrice}
                        shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
                        onSuccess={(details, data) => {
                            console.log("pay pal resp", details, data)
                            let paymentDetails = {
                                facilitatorAccessToken: data.facilitatorAccessToken,
                                orderID: data.orderID,
                                payerID: data.payerID,
                                price: this.state.recordPrice
                            }
                            this.props.onClose(paymentDetails)
                            // alert("Transaction completed by " + details.payer.name.given_name);

                            // OPTIONAL: Call your server to save the transaction
                            // return fetch("/paypal-transaction-complete", {
                            //   method: "post",
                            //   body: JSON.stringify({
                            //     orderID: data.orderID
                            //   })
                            // });
                        }}
                    />
                </CustomModal>
            </React.Fragment>
        );
    }
}

export default PaymentModal;