import React, { Component } from 'react';
import CustomModal from './common/CustomModal';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts';
import { httpClient } from '../UtilService';
import CustomLoading from './Loading'
class OfferApproveModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOffer: props.selectedOffer,
            data: []
        }
    }
    componentDidMount() {
        this.getOfferSubmissions()
    }

    getOfferSubmissions = async () => {

        let result = await httpClient(`offer/${this.props.selectedOffer._id}/submissions`, "GET");
        console.log(result);
        if (result.success) {
            this.setState({ data: result.data, loading: false });
            console.log(this.state.data);
        } else {
            this.setState({
                loading: false
            })
            ToastsStore.error(result.message);
        }
    }
    onClose = (isPurchased) => {
        this.props.onClose(isPurchased)
    }

    handleApproveSubmission = async (submission) => {

        let result = await httpClient(`offer/${submission.offerId}/buy`, "GET", { patientId: submission.userId._id });
        console.log(result);
        if (result.success) {
            this.setState({ loading: false },
                () => {
                    this.onClose(true)
                });
            console.log(this.state.data);
        } else {
            this.setState({
                loading: false
            })
            ToastsStore.error(result.message);
        }
    }
    render() {
        return (
            <React.Fragment>
                <CustomModal size="lg" title="Send Request" onClose={() => this.onClose()} >
                    <ToastsContainer
                        store={ToastsStore}
                        position={ToastsContainerPosition.TOP_RIGHT}
                    />
                    {this.state.loading && (
                        <CustomLoading />
                    )}
                    <div className="row m-0 align-items-end mb-3">
                        <div className="col-12">
                            <div className="title">
                                <h6>Offer Price - <strong>${this.state.selectedOffer.price}</strong></h6>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="table-responsive">
                                <table className="table table-bordered">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">User</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.data.map((submission, index) => (
                                            <tr key={index}>
                                                <td>{submission.userId.name}({submission.userId.email})</td>
                                                <td>{submission.price}</td>
                                                <td>
                                                    <button onClick={() => this.handleApproveSubmission(submission)} type="button" className="btn btn-outline-primary">
                                                        Buy Offer
                                                            </button>
                                                </td>
                                            </tr>
                                        ))}
                                        {this.state.data.length === 0 && (
                                            <tr>
                                                <td className="text-center" colSpan="3">No Offers found</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </CustomModal>
            </React.Fragment>
        );
    }
}

export default OfferApproveModal;