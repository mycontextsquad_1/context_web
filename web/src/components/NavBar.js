import React, { Component } from "react";
import "../css/NavBar.css";
import { Button, UncontrolledPopover, PopoverHeader, PopoverBody, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap'

class NavBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user_type: localStorage.getItem("user_type"),
      userName: localStorage.getItem("name"),
      isDropDownOpen: false
    };
  }

  logout = event => {
    localStorage.removeItem("access-token");
    localStorage.removeItem("name");
    localStorage.removeItem("email");
    localStorage.removeItem("user_type");
    this.props.history.push("/");
  };

  navigateTo = (link) => {
    this.props.history.push(link);
  }

  toggle = () => {
    this.setState({
      isDropDownOpen: !this.state.isDropDownOpen
    });
  }

  render() {
    return (
      <nav className="navbar d-flex fixed-top flex-md-nowrap p-0 shadow">
        <a className="navbar-brand col-sm-3 col-md-2 mr-0" href="/">
          <img src="/images/logo-new.png" alt="logo" />
        </a>
        <div className="flex-grow-1">
          <input
            className="form-control w-100"
            type="text"
            placeholder="Search"
            aria-label="Search"
          />
        </div>
        <div className="d-none d-md-block">
          {this.state.user_type !== "Patient" && (
            <button className="btn" onClick={() => this.navigateTo("/")}>
              Home
            </button>
          )}
          <button className="btn" onClick={() => this.navigateTo("/records")}>
            My Records
        </button>
          {/* {this.state.user_type !== "Patient" && (
            <button className="btn" onClick={() => this.navigateTo("/add")}>
              Add Record
            </button>
          )} */}

        </div>
        <ul className="navbar-nav px-3">
          <li className="nav-item text-nowrap d-md-none">
            <a className="nav-link" href="/records">
              My Records
            </a>
          </li>
          <li className={["Doctor", "Hospital"].indexOf(this.state.user_type) === -1 ? "d-none" : "nav-item text-nowrap d-md-none"}>
            <a className="nav-link" href="/add">
              Add Record
            </a>
          </li>
          <li className="nav-item text-nowrap">
            {/* <a className="nav-link hand" onClick={this.logout}>
              Sign out
            </a> */}

            <Dropdown nav isOpen={this.state.isDropDownOpen} toggle={this.toggle}>
              <DropdownToggle nav caret className="user-drop-down">
                <i className="far fa-user mr-1"></i> {this.state.userName}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem onClick={() => this.navigateTo("/profile-details")}>Profile Details</DropdownItem>
                {this.state.user_type === "Admin" && (
                  <DropdownItem onClick={() => this.navigateTo("/users")}>Users</DropdownItem>
                )}
                {["Doctor", "Hospital"].indexOf(this.state.user_type) > -1 && (
                  <DropdownItem onClick={() => this.navigateTo("/request-patients")}>Request Patients</DropdownItem>
                )}
                {["Pharmaceutical Company", "Pathology Laboratory"].indexOf(this.state.user_type) > -1 && (
                  <DropdownItem onClick={() => this.navigateTo("/offers")}>Offers</DropdownItem>
                )}
                <DropdownItem onClick={() => this.navigateTo("/wallet")}>Wallet</DropdownItem>
                {this.state.user_type === "Patient" && (
                  <React.Fragment>
                    <DropdownItem onClick={() => this.navigateTo("/doctor-requests")}>Doctor Requests</DropdownItem>
                    <DropdownItem onClick={() => this.navigateTo("/patient-offers")}>Bid Offers</DropdownItem>
                  </React.Fragment>
                )}
                <DropdownItem onClick={this.logout}>Sign out</DropdownItem>
              </DropdownMenu>
            </Dropdown>

          </li>
        </ul>
      </nav>
    );
  }
}

export default NavBar;
