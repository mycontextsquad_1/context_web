import React, { Component } from 'react';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts';
import NavBar from './NavBar'
import CustomLoading from './Loading';
import { httpClient } from '../UtilService';
class ProfileDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isEdit: false,
            isDataLoaded: false,
            userDetails: {
                organization: "",
                designation: "",
                phone: "",
                address: ""
            }
        }
    }
    componentDidMount() {
        document.title = "Profile Details";
        // document.body.classList.add("white");
        this.getUserProfileDetails();
    }
    getUserProfileDetails = async () => {
        this.setState({
            isLoading: true
        })
        let result = await httpClient("user/profile-details", "GET");
        if (result && result.success && result.data) {
            this.setState(prevState => ({
                isLoading: false,
                userDetails: {
                    ...prevState.userDetails,
                    ...result.data
                },
                isDataLoaded: true,
                isEdit: result.data ? false : true
            }))
        } else {
            this.setState({
                isLoading: false,
                isDataLoaded: true,
                isEdit: result.data ? false : true
            })
        }
        console.log("result", result)

    }

    toggleIsEdit = () => {
        this.setState({
            isEdit: !this.state.isEdit
        })
    }
    handleProfileUpdate = async () => {
        console.log("details", this.state.userDetails)
        this.setState({
            isLoading: true
        })
        let result = await httpClient("user/profile-details", "PATCH", this.state.userDetails);
        this.setState({
            isLoading: false
        })
        if (result && result.success) {
            ToastsStore.success("Details Updated.");
            this.getUserProfileDetails()
        } else {
            ToastsStore.error("Error while updating.");
        }
    }

    handleChange = event => {
        let eve = { ...event };
        this.setState(prevState => ({
            userDetails: {
                ...prevState.userDetails,
                [eve.target.name]: eve.target.value
            }
        }));
    };
    render() {
        return (
            <React.Fragment>
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                {this.state.isLoading && <CustomLoading />}
                <NavBar {...this.props} />
                <br />
                <div className="row m-0 mt-4">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <br />
                        <div className="card">
                            <div className="card-body">
                                <form>
                                    <h5 className="text-center">Profile Details</h5>
                                    {this.state.isDataLoaded && (
                                        this.state.isEdit ? (
                                            <React.Fragment>
                                                <div className="form-group">
                                                    <label htmlFor="organization">Organization</label>
                                                    <input
                                                        type="text"
                                                        id="organization"
                                                        className="form-control"
                                                        placeholder="Organization"
                                                        name="organization"
                                                        value={this.state.userDetails.organization}
                                                        onChange={this.handleChange}
                                                        autoFocus
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="designation">Designation</label>
                                                    <input
                                                        type="text"
                                                        id="designation"
                                                        className="form-control"
                                                        placeholder="Designation"
                                                        name="designation"
                                                        value={this.state.userDetails.designation}
                                                        onChange={this.handleChange}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="phone">Phone</label>
                                                    <input
                                                        type="number"
                                                        id="phone"
                                                        className="form-control"
                                                        placeholder="Phone"
                                                        name="phone"
                                                        value={this.state.userDetails.phone}
                                                        onChange={this.handleChange}
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="address">Address</label>
                                                    <textarea
                                                        type="text"
                                                        id="address"
                                                        rows="5"
                                                        className="form-control"
                                                        placeholder="Address"
                                                        name="address"
                                                        value={this.state.userDetails.address}
                                                        onChange={this.handleChange}
                                                    ></textarea>
                                                </div>
                                                <div className="text-center mt-3">
                                                    <button type="button" className="btn btn-outline-primary" onClick={this.getUserProfileDetails}>Cancel</button> {" "}
                                                    <button type="button" className="btn btn-primary" onClick={this.handleProfileUpdate}>Save</button>
                                                </div>
                                            </React.Fragment>
                                        ) : (
                                                <React.Fragment>
                                                    <div className="row">
                                                        <div className="col-12 mb-2">
                                                            <strong>Organization :</strong> {this.state.userDetails.organization}
                                                        </div>
                                                        <div className="col-12 mb-2">
                                                            <strong>Designation :</strong> {this.state.userDetails.designation}
                                                        </div>
                                                        <div className="col-12 mb-2">
                                                            <strong>Phone :</strong> {this.state.userDetails.phone}
                                                        </div>
                                                        <div className="col-12 mb-2">
                                                            <strong>Address :</strong> {this.state.userDetails.address}
                                                        </div>
                                                        <div className="col-12 text-right">
                                                            <br />
                                                            <button type="button" className="btn btn-primary" onClick={this.toggleIsEdit}>Edit</button>
                                                        </div>
                                                    </div>
                                                </React.Fragment>
                                            )
                                    )}

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default ProfileDetails;