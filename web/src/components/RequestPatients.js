import React, { Component } from "react";
import NavBar from "./NavBar";
import {
    ToastsContainer,
    ToastsStore,
    ToastsContainerPosition
} from "react-toasts";

import { httpClient } from '../UtilService'
import CustomLoading from "./Loading";

class RequestPatients extends Component {
    userId = ""
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
            patientAccess: {}
        };
    }

    componentDidMount() {
        document.title = "Request Patients";
        this.userId = localStorage.getItem("user_id")
        this.getRequestPatients();
    }

    getRequestPatients = async () => {
        var self = this;
        let result = await httpClient("user/doctor-request", "GET");
        let patientResult = await httpClient("user/list-users", "POST", {});
        console.log(result);
        if (patientResult.success) {
            let patientAccess = result.data.reduce((acc, ele) => {
                acc[ele.patientId] = ele.hasAccepted;
                return acc;
            }, {})
            self.setState({ data: patientResult.data, isLoading: false, patientAccess });
        } else {
            this.setState({
                isLoading: false
            })
            ToastsStore.error(result.message);
        }
    }

    handleChange = (event, value) => {
        let eve = { ...event }
        this.setState(prevState => ({
            search: {
                ...prevState.search,
                [eve.target.name]: value ? value : eve.target.value
            }
        }))
    }


    requestPatientForAccess = async (data) => {
        let self = this;
        self.setState({ isLoading: true });

        var result = await httpClient("user/doctor-request/patient", "PUT", { patientId: data._id });
        if (result.success) {
            let patientAccess = { ...this.state.patientAccess };
            patientAccess[data._id] = false
            self.setState({
                isLoading: false,
                patientAccess
            })
            ToastsStore.success("Request sent.");
        } else {
            self.setState({ isLoading: false });
            ToastsStore.error(result.message);
        }

    }

    render() {
        return (
            <React.Fragment>
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <NavBar {...this.props} />
                <div className="container-fluid">
                    <div className="row">
                        <div
                            role="main"
                            className="col-12 main-container pt-60 mobile-space"
                        >
                            <h2>Patients</h2>
                            <hr />
                            {this.state.isLoading && (
                                <CustomLoading />
                            )}
                            <div className="table-responsive bg-white">
                                <table className="table table-bordered">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">Patient</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.data.map((patient, index) => (
                                            <tr key={index}>
                                                <td>{patient.name}</td>
                                                <td>{patient.email}</td>
                                                <td>

                                                    {this.state.patientAccess[patient._id] === true ? (
                                                        <React.Fragment>Accepted</React.Fragment>
                                                    ) : (
                                                            <React.Fragment>
                                                                {this.state.patientAccess[patient._id] === false ? (
                                                                    <React.Fragment>Waiting for acceptance</React.Fragment>
                                                                ) : (
                                                                        <button onClick={() => this.requestPatientForAccess(patient)} type="button" className="btn btn-primary">
                                                                            Request
                                                                        </button>
                                                                    )}
                                                            </React.Fragment>
                                                        )}


                                                </td>
                                            </tr>
                                        ))}
                                        {this.state.data.length === 0 && (
                                            <tr>
                                                <td className="text-center" colSpan="8">No Patients found</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

export default RequestPatients;
