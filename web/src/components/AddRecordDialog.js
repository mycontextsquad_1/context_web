import React, { Component } from 'react';
import CustomModal from './common/CustomModal';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts';
import { httpClient } from '../UtilService';
import CustomLoading from './Loading'
import { CANCER_TYPES } from '../common';
class AddRecordDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            users: [],
            data: {
                cs_tumor_size: 5,
                cancer_type: "mouth",
                gender: "Male"
            },
            patient_id: "",
            extraFields: [
                { label: "Price", property: "price", value: "" },
                { label: "GP Practice", property: "gp_practice", value: "" },
                { label: "GP Practice Address", property: "gp_practice_address", value: "" },
                { label: "Behavior Code ICD", property: "behavior_code", value: "" },
                { label: "Behavior Recode for Analysis", property: "behavior_recode_for_analysis", value: "" },
                { label: "Breast Adjusted AJCC 6th Stage", property: "breast_adjusted_ajcc_6_stage", value: "" },
                { label: "Breast Adjusted AJCC 6th M", property: "breast_adjusted_ajcc_6m", value: "" },
                { label: "Breast Adjusted AJCC 6th N", property: "breast_adjusted_ajcc_6n", value: "" },
                { label: "Breast Adjusted AJCC 6th T", property: "breast_adjusted_ajcc_6t", value: "" },
                { label: "CS Lymph Nodes", property: "cs_lymph_nodes", value: "" },
                { label: "CS Lymph Nodes Eval", property: "cs_lymph_nodes_eval", value: "" },
                { label: "CS Mets at Dx", property: "cs_mets_at_dx", value: "" },
                { label: "CS Mets at Dx Bone", property: "cs_mets_at_dx_bone", value: "" },
                { label: "CS Mets at Dx Brain", property: "cs_mets_at_dx_brain", value: "" },
                { label: "CS Mets at Dx Liver", property: "cs_mets_at_dx_liver", value: "" },
                { label: "CS Mets at Dx Lung", property: "cs_mets_at_dx_lung", value: "" },
                { label: "CS Mets Eval", property: "cs_mets_eval", value: "" },
                { label: "CS Schema -AJCC 6th ed", property: "cs_schema_ajcc_6", value: "" },
                { label: "CS Schema v0204+", property: "cs_schema_v0204", value: "" },
                { label: "CS Tumor Size/Ext Eval", property: "cs_tumor_size_ext_eval", value: "" },
                { label: "Derived AJCC", property: "derived_ajcc", value: "" },
                { label: "Derived AJCC 6th T", property: "derived_ajcc_6_m", value: "" },
                { label: "Derived AJCC 6th N", property: "derived_ajcc_6_n", value: "" },
                { label: "Derived AJCC 6th Stage Group", property: "derived_ajcc_6_stage_grp", value: "" },
                { label: "Derived AJCC 6th T", property: "derived_ajcc_6_t", value: "" },
                { label: "Derived AJCC 7th Stage Group", property: "derived_ajcc_7_stage_grp", value: "" },
                { label: "Derived AJCC 7th M", property: "derived_ajcc_7m", value: "" },
                { label: "Derived AJCC 7th N", property: "derived_ajcc_7n", value: "" },
                { label: "Derived AJCC 7th T", property: "derived_ajcc_7t", value: "" },
                { label: "Diagnostic Confirmation", property: "diagnostic_confirmation", value: "" },
                { label: "ER Status Recode Breast Cancer", property: "er_status_recode_breast_cancer", value: "" },
                { label: "First Malignant Primary Indicator", property: "first_maligant_primary_indicator", value: "" },
                { label: "Grade", property: "grade", value: "" },
                { label: "Histology Recode—Brain Groupings", property: "histology_recode_brain_groupings", value: "" },
                { label: "Histology Recode—Broad Groupings", property: "histology_recode_broad_groupings", value: "" },
                { label: "Marital Status", property: "marital_status", value: "" },
                { label: "Month of Diagnosis", property: "month_of_diagnosis", value: "" },
                { label: "Origin recode NHIA (Hispanic, Non-Hisp)", property: "origin_recode_niha", value: "" },
                { label: "Primary by International Rules", property: "primary_by_internationals_rules", value: "" },
                { label: "Primary Site", property: "primary_site", value: "" },
                { label: "Reason for no Surgery", property: "reason_for_no_surgery", value: "" },
                { label: "Regional Nodes Examined", property: "regional_nodes_examined", value: "" },
                { label: "Regional Nodes Positive", property: "regional_nodes_positive", value: "" },
                { label: "RX Summ—Surg Prim Site", property: "rx_summ", value: "" },
                { label: "RX Summ—Scope Reg LN Sur", property: "rx_summ_scope_reg_ln_sur", value: "" },
                { label: "RX Summ—Surg Oth Reg/Dis", property: "rx_summ_surg_oth_reg_or_dis", value: "" },
                { label: "Sequence Number", property: "sequence_number", value: "" },
                { label: "Country", property: "country", value: "" },
                { label: "State County Recode", property: "state_county_recode", value: "" },
                { label: "Survival Months", property: "survival_months", value: "" },
                { label: "Total Number of Benign/Borderline Tumors", property: "total_benign_tumors", value: "" },
                { label: "Total Number of In Situ/malignant Tumors", property: "total_situ_tumors", value: "" },
                { label: "Tumor Marker 1", property: "tumor_marker_1", value: "" },
                { label: "Tumor Marker 2", property: "tumor_marker_2", value: "" },
                { label: "Tumor Marker 3", property: "tumor_marker_3", value: "" },
                { label: "Type of Reporting Source", property: "type_of_reporting_source", value: "" },
                { label: "Vital Status recode", property: "vital_status_code", value: "" },
                { label: "Year of Birth", property: "year_of_birth", value: "" },
                { label: "Year of Diagnosis", property: "year_of_diagnosis", value: "" },
                { label: "Others", property: "others", value: "" },
            ],
            extraFieldsAdded: [],
            isShowAddMoreFields: true,
            currentAddingFields: {
                property: "",
                value: ""
            }
        }
    }
    componentDidMount() {
        this.getUsers()
    }
    getUsers = async () => {
        this.setState({
            loading: true
        })
        let result = await httpClient("user/doctor-request/patient-accepted", "GET");
        console.log(result);
        if (result.success) {
            let data = result.data.map(x => {
                return {
                    _id: x.patientId._id,
                    name: x.patientId.name,
                    email: x.patientId.email,
                }
            })
            this.setState({ users: data, loading: false });
        } else {
            this.setState({
                loading: false
            })
            ToastsStore.error(result.message);
        }
    }
    onClose = () => {
        this.props.onClose()
    }

    addRecord = async (event) => {
        event.preventDefault();
        if (this.state.patient_id !== undefined && this.state.patient_id === "") {
            ToastsStore.error("Please select patient");
        } else {
            let extraAddedData = this.state.extraFieldsAdded.reduce((acc, ele) => {
                acc[ele.property] = ele.value
                return acc
            }, {})
            this.setState({ loading: true });
            let patient = JSON.parse(this.state.patient_id)
            var payload = {
                data: { ...this.state.data, ...extraAddedData, name: patient.name, patient_id: patient._id },
                patient_id: patient._id
            };
            let result = await httpClient("record/addRecord", "POST", payload);
            if (result.success) {
                ToastsStore.success("Record created successfully.");
                console.log(this.state.data);
                this.props.onClose(true)
            } else {
                this.setState({
                    loading: false
                })
                ToastsStore.error(result.message);
            }
        };

    }
    handleChange = event => {
        const dataCopy = this.state.data;
        dataCopy[event.target.name] = event.target.value;
        this.setState({ data: dataCopy });
    };

    handleExtraFieldChangeChange = (event) => {
        let eve = { ...event }
        this.setState(prevState => ({
            currentAddingFields: {
                ...prevState.currentAddingFields,
                [eve.target.name]: eve.target.value
            }
        }))
    }

    handleExtraAddedFieldChange = (event, fieldIndex) => {
        let extraFields = [...this.state.extraFieldsAdded];
        extraFields[fieldIndex].value = event.target.value;
        this.setState({
            extraFieldsAdded: extraFields
        })
    }

    toggleShowMoreFields = () => {
        this.setState({
            isShowAddMoreFields: !this.state.isShowAddMoreFields
        })
    }

    handleOptionChange = event => {
        let eve = { ...event }
        this.setState({
            [eve.target.name]: event.target.value,
        });
    };

    addExtraField = () => {
        if (!this.state.currentAddingFields.property) {
            ToastsStore.error("Select Field to add");
        } else if (!this.state.currentAddingFields.value) {
            ToastsStore.error("Enter value to add");
        } else {
            let fieldIndex = this.state.extraFields.findIndex(x => x.property === this.state.currentAddingFields.property);
            if (fieldIndex > -1) {
                let extraFieldsAdded = [...this.state.extraFieldsAdded];
                let field = this.state.extraFields[fieldIndex]
                let extraFields = [...this.state.extraFields];
                extraFields.splice(fieldIndex, 1);
                extraFieldsAdded.push({ label: field.label, property: this.state.currentAddingFields.property, value: this.state.currentAddingFields.value });
                this.setState({
                    extraFields,
                    extraFieldsAdded,
                    isShowAddMoreFields: true,
                    currentAddingFields: {
                        property: "",
                        value: ""
                    }
                })
            }
        }
    }
    cancelAddExtraField = () => {
        this.setState({
            currentAddingFields: {
                property: "",
                value: ""
            },
            isShowAddMoreFields: true
        })
    }
    removeExtraFieldAdded = (extraFieldIndex) => {
        let extraFieldsAdded = [...this.state.extraFieldsAdded];
        let extraFieldAdded = { ...extraFieldsAdded[extraFieldIndex] };
        let extraFields = [...this.state.extraFields];
        extraFields.unshift({ label: extraFieldAdded.label, property: extraFieldAdded.value, value: "" })
        extraFieldsAdded.splice(extraFieldIndex, 1);
        this.setState({
            extraFields,
            extraFieldsAdded
        })
    }
    render() {
        return (
            <React.Fragment>
                <CustomModal size="lg" title="Add Record" onClose={() => this.onClose()} >
                    <ToastsContainer
                        store={ToastsStore}
                        position={ToastsContainerPosition.TOP_RIGHT}
                    />
                    {this.state.loading && (
                        <CustomLoading />
                    )}
                    {this.state.users.length > 0 ? (


                        <form onSubmit={this.addRecord}>
                            <div className="row m-0">
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label>Patient</label>
                                        <select
                                            className="form-control"
                                            name="patient_id"
                                            value={this.state.patient_id}
                                            onChange={this.handleOptionChange}
                                        >
                                            <option value="" defaultValue>
                                                Select patient
                          </option>
                                            {this.state.users.map((user, key) => {
                                                return (
                                                    <option key={key} value={JSON.stringify(user)}>
                                                        {user.name}{`<${user.email}>`}
                                                    </option>
                                                );
                                            })}
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="inputPrice">Price</label>
                                        <input
                                            type="number"
                                            id="inputPrice"
                                            className="form-control"
                                            placeholder="Price"
                                            name="price"
                                            value={this.state.data.price}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group mb-0">
                                        <label>Cancer Type</label>
                                        <select required className="form-control" name="cancer_type" value={this.state.data.cancer_type} onChange={this.handleChange}>
                                            {CANCER_TYPES.map(x => (
                                                <option value={x.label} key={x.label}> {x.value}</option>
                                            ))}
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="age_at_diagnosis">
                                            Age at Diagnosis
                                </label>
                                        <input
                                            type="text"
                                            id="age_at_diagnosis"
                                            className="form-control"
                                            placeholder="Age at Diagnosis"
                                            name="age_at_diagnosis"
                                            value={this.state.data.age_at_diagnosis}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label>Tumor Size</label>
                                        <select required className="form-control" name="cs_tumor_size" value={this.state.data.cs_tumor_size} onChange={this.handleChange}>
                                            {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(x => (
                                                <option value={x} key={x}> {x}</option>
                                            ))}
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label>Gender</label>
                                        <select className="form-control" name="gender" value={this.state.data.gender} onChange={this.handleChange}>
                                            <option value="Male"> Male</option>
                                            <option value="Female"> Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="year_of_birth">Year of Birth</label>
                                        <input
                                            type="text"
                                            id="year_of_birth"
                                            className="form-control"
                                            placeholder="Year of Birth"
                                            name="year_of_birth"
                                            value={this.state.data.year_of_birth}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="year_of_diagnosis">
                                            Year of Diagnosis
                        </label>
                                        <input
                                            type="text"
                                            id="year_of_diagnosis"
                                            className="form-control"
                                            placeholder="Year of Diagnosis"
                                            name="year_of_diagnosis"
                                            value={this.state.data.year_of_diagnosis}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>


                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="weight">Weight</label>
                                        <input
                                            type="text"
                                            id="weight"
                                            className="form-control"
                                            placeholder="Weight"
                                            name="weight"
                                            value={this.state.data.weight}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="blood_pressure">Blood Pressure</label>
                                        <input
                                            type="text"
                                            id="blood_pressure"
                                            className="form-control"
                                            placeholder="Blood Pressure"
                                            name="blood_pressure"
                                            value={this.state.data.blood_pressure}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>

                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="blood_group">Blood Group</label>
                                        <input
                                            type="text"
                                            id="blood_group"
                                            className="form-control"
                                            placeholder="Blood Group"
                                            name="blood_group"
                                            value={this.state.data.blood_group}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="visit_date">Visit Date</label>
                                        <input
                                            type="date"
                                            id="visit_date"
                                            className="form-control"
                                            placeholder="Visit Date"
                                            name="visit_date"
                                            value={this.state.data.visit_date}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="outcome_date">Outcome Date</label>
                                        <input
                                            type="date"
                                            id="outcome_date"
                                            className="form-control"
                                            placeholder="Outcome Date"
                                            name="outcome_date"
                                            value={this.state.data.outcome_date}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="form-group">
                                        <label htmlFor="outcome">Outcome</label>
                                        <input
                                            type="text"
                                            id="outcome"
                                            className="form-control"
                                            placeholder="Outcome"
                                            name="outcome"
                                            value={this.state.data.outcome}
                                            onChange={this.handleChange}
                                            required
                                        />
                                    </div>
                                </div>
                                {this.state.extraFieldsAdded.map((field, fieldIndex) => (
                                    <div className="col-md-6 col-12" key={fieldIndex}>
                                        <div className="form-group">
                                            <label htmlFor={field.property}>
                                                {field.label}
                                                <span className="remove-extra-field" onClick={() => this.removeExtraFieldAdded(fieldIndex)}>Remove</span>
                                            </label>
                                            <input
                                                type="text"
                                                id={field.property}
                                                className="form-control"
                                                placeholder={field.label}
                                                name={field.property}
                                                value={field.value}
                                                onChange={(e) => this.handleExtraAddedFieldChange(e, fieldIndex)}
                                                required
                                            />
                                        </div>
                                    </div>
                                ))}
                                <div className="col-12">
                                    {this.state.isShowAddMoreFields ? (
                                        this.state.extraFields.length > 0 && (
                                            <button
                                                className="btn btn-sm btn-primary"
                                                type="button"
                                                onClick={this.toggleShowMoreFields}
                                            >
                                                Add More Fields
                                            </button>
                                        )
                                    ) : (
                                            <React.Fragment>
                                                <div className="row align-items-end m-0">
                                                    <div className="form-group mr-2 mb-0">
                                                        <label>Field</label>
                                                        <select className="form-control" name="property" value={this.state.currentAddingFields.property} onChange={this.handleExtraFieldChangeChange}>
                                                            <option value=""></option>
                                                            {this.state.extraFields.map(x => (
                                                                <option value={x.property} key={x.property}> {x.label}</option>
                                                            ))}
                                                        </select>
                                                    </div>
                                                    <div className="form-group mr-2 mb-0">
                                                        <label htmlFor="value">Value</label>
                                                        <input
                                                            type="text"
                                                            id="value"
                                                            className="form-control"
                                                            placeholder="Value"
                                                            name="value"
                                                            value={this.state.currentAddingFields.value}
                                                            onChange={this.handleExtraFieldChangeChange}
                                                            required
                                                        />
                                                    </div>
                                                    <button
                                                        className="btn btn-sm btn-primary mr-2"
                                                        type="button"
                                                        onClick={this.addExtraField}
                                                    >
                                                        Add
                                    </button>
                                                    <button
                                                        className="btn btn-sm btn-outline-primary"
                                                        type="button"
                                                        onClick={this.cancelAddExtraField}
                                                    >
                                                        Cacel
                                    </button>

                                                </div>

                                            </React.Fragment>
                                        )}
                                </div>
                                <div className="col-12 text-right">
                                    <br />
                                    <button
                                        className="btn btn-sm btn-primary"
                                        type="submit"
                                    >
                                        Create
                      </button>
                                </div>

                            </div>
                        </form>
                    ) : (
                            <React.Fragment>
                                <div className="text-center">
                                    <h4>
                                        No patients found.
                                    </h4>
                                    <h6>Please request patients to add a records</h6>
                                </div>
                            </React.Fragment>
                        )}
                </CustomModal>
            </React.Fragment >
        );
    }
}

export default AddRecordDialog;