import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap'
const CustomModal = (props) => {
    return (
        <React.Fragment>
            <Modal size={props.size || 'sm'} isOpen={true} backdrop={true} toggle={props.onClose} className={props.className ? props.className : ""}>
                {props.title && (
                    <ModalHeader toggle={props.onClose}>{props.title}</ModalHeader>
                )}
                <ModalBody>
                    {props.children}
                </ModalBody>
            </Modal>
        </React.Fragment>
    );
}

export default CustomModal;