import React, { Component } from 'react';
class VerificationPage extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        return (
            <section className=" padding-16">
                <div className="row">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto mtb">
                        <br />
                        <br />
                        <div className="card card-signin">
                            <div className="card-body text-center">
                                <img src="/images/email.png" className="mb-4" alt="email" />
                                <h5 className="m-0 text-center">
                                    A verification email sent to your mail
                             </h5>
                                <p>Please verify your email</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
}

export default VerificationPage;