import React, { Component } from 'react';
import CustomModal from './common/CustomModal';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts';
class OfferRequestModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedOffer: props.selectedOffer,
            yourPrice: ""
        }
    }
    handleChange = (event, value) => {
        let eve = { ...event }
        this.setState(prevState => ({
            [eve.target.name]: value ? value : eve.target.value
        }))
    }
    onClose = (isSend) => {
        if (isSend) {
            if (!this.state.yourPrice || this.state.yourPrice < 0) {
                ToastsStore.error("Enter valid price");
            } else {
                this.props.onClose({ ...this.state.selectedOffer, price: this.state.yourPrice })
            }
        } else {
            this.props.onClose()
        }
    }
    render() {
        return (
            <React.Fragment>
                <CustomModal title="Send Request" onClose={() => this.onClose()} >
                    <ToastsContainer
                        store={ToastsStore}
                        position={ToastsContainerPosition.TOP_RIGHT}
                    />
                    <div className="row m-0 align-items-end mb-3">
                        <div className="col-12">
                            <div className="title">
                                <h6>Offer Price - <strong>${this.state.selectedOffer.price}</strong></h6>
                            </div>
                        </div>
                        <div className="col-12">
                            <div className="form-group mb-0 mr-2">
                                <label>Your Price($)</label>
                                <input type="number"
                                    id="yourPrice"
                                    className="form-control"
                                    placeholder="Your Price"
                                    name="yourPrice"
                                    value={this.state.yourPrice}
                                    onChange={this.handleChange}
                                />
                            </div>
                        </div>
                        <div className="col-12 text-right">
                            <br />
                            <button onClick={() => this.onClose(true)} type="button" className="btn btn-primary">
                                Send Request
                                                            </button>
                        </div>
                    </div>
                </CustomModal>
            </React.Fragment>
        );
    }
}

export default OfferRequestModal;