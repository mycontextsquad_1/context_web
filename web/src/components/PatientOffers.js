import React, { Component } from "react";
import NavBar from "./NavBar";
import {
    ToastsContainer,
    ToastsStore,
    ToastsContainerPosition
} from "react-toasts";

import { httpClient } from '../UtilService'
import CustomLoading from "./Loading";
import { CANCER_TYPES, AGE_GROUPS } from "../common";
import OfferRequestModal from "./OfferRequestModal";

class PatientOffers extends Component {
    userId = ""
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            loading: true,
            search: {
                cancerType: "all",
                tumorSize: "all",
                ageGroup: "all",
                gender: "all"
            },
            offerRequestModal: {
                isOpen: false,
                data: undefined
            }
        };
    }

    componentDidMount() {
        document.title = "Patient Offers";
        this.userId = localStorage.getItem("user_id")
        this.getPatientOffers();
    }

    getPatientOffers = async () => {
        var self = this;
        let search = { ...this.state.search };
        if (search.cancerType === "all") {
            delete search.cancerType;
        }
        if (search.tumorSize === "all") {
            delete search.tumorSize;
        }
        if (search.ageGroup === "all") {
            delete search.ageGroup;
        }
        if (search.gender === "all") {
            delete search.gender;
        }
        let result = await httpClient("offer/patient-offers", "POST", search);
        console.log(result);

        if (result.success) {
            self.setState({ data: result.data, loading: false });
            console.log(self.state.data);
        } else {
            this.setState({
                loading: false
            })
            ToastsStore.error(result.message);
        }
    }

    handleChange = (event, value) => {
        let eve = { ...event }
        this.setState(prevState => ({
            search: {
                ...prevState.search,
                [eve.target.name]: value ? value : eve.target.value
            }
        }))
    }

    handleSendRequestModal = (offer) => {
        this.setState({
            offerRequestModal: {
                isOpen: true,
                data: offer
            }
        });
    }

    handleClose = async (data) => {
        if (data) {
            let self = this;
            self.setState({ loading: true });

            var result = await httpClient("offer/send-request", "PUT", data);
            console.log("payment ", result);
            if (result.success) {
                self.setState({
                    offerRequestModal: {
                        isOpen: false,
                        data: undefined
                    },
                    loading: false
                }, self.getPatientOffers)
                ToastsStore.success("Request sent.");
            } else {
                self.setState({ loading: false });
                ToastsStore.error(result.message);
            }
        } else {
            this.setState({
                offerRequestModal: {
                    isOpen: false,
                    data: undefined
                }
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <NavBar {...this.props} />
                <div className="container-fluid">
                    <div className="row">
                        <div
                            role="main"
                            className="col-12 main-container pt-60 mobile-space"
                        >
                            <h2>Offers</h2>
                            <hr />
                            <div className="row m-0 align-items-end mb-3">
                                <div className="form-group mb-0 mr-2">
                                    <label>Cancer Type</label>
                                    <select className="form-control" name="cancerType" value={this.state.search.cancerType} onChange={this.handleChange}>
                                        <option value="all">All</option>
                                        {CANCER_TYPES.map(x => (
                                            <option value={x.label} key={x.label}> {x.value}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group mb-0 mr-2">
                                    <label>Tumor Size</label>
                                    <select className="form-control" name="tumorSize" value={this.state.search.tumorSize} onChange={this.handleChange}>
                                        <option value="all">All</option>
                                        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(x => (
                                            <option value={x} key={x}> {x}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group mb-0 mr-2">
                                    <label>Age(Years)</label>
                                    <select className="form-control" name="ageGroup" value={this.state.search.ageGroup} onChange={this.handleChange}>
                                        <option value="all">All</option>
                                        {AGE_GROUPS.map(x => (
                                            <option value={x} key={x}> {x}</option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group mb-0 mr-2">
                                    <label>Gender</label>
                                    <select className="form-control" name="gender" value={this.state.search.gender} onChange={this.handleChange}>
                                        <option value="all">All</option>
                                        <option value="Male"> Male</option>
                                        <option value="Female"> Female</option>
                                    </select>
                                </div>
                                <div>
                                    <button className="btn btn-primary" onClick={this.getPatientOffers}>Search</button>
                                </div>
                            </div>
                            {this.state.loading && (
                                <CustomLoading />
                            )}
                            <div className="table-responsive bg-white">
                                <table className="table table-bordered">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">Description</th>
                                            <th scope="col">Cancer Type</th>
                                            <th scope="col">Age Group(Years)</th>
                                            <th scope="col">Price</th>
                                            <th scope="col">Tumor Size</th>
                                            <th scope="col">Gender</th>
                                            <th scope="col">Period</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.data.map((offer, index) => (
                                            <tr key={index}>
                                                <td>{offer.description}</td>
                                                <td>{offer.cancerType}</td>
                                                <td>{offer.ageGroup}</td>
                                                <td>{offer.price}</td>
                                                <td>{offer.tumorSize}</td>
                                                <td>{offer.gender}</td>
                                                <td>{new Date(offer.startDate).toDateString()} - {new Date(offer.endDate).toDateString()}</td>
                                                <td>
                                                    {offer.requestedBy.indexOf(this.userId) > -1 ? (
                                                        <React.Fragment>Request submitted</React.Fragment>
                                                    ) : (

                                                            <button onClick={() => this.handleSendRequestModal(offer)} type="button" className="btn btn-primary">
                                                                BID
                                                            </button>
                                                        )}
                                                </td>
                                            </tr>
                                        ))}
                                        {this.state.data.length === 0 && (
                                            <tr>
                                                <td className="text-center" colSpan="8">No Offers found</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {
                    this.state.offerRequestModal.isOpen && (
                        <React.Fragment>
                            <OfferRequestModal selectedOffer={this.state.offerRequestModal.data} onClose={this.handleClose} />
                        </React.Fragment>
                    )
                }
            </React.Fragment >
        );
    }
}

export default PatientOffers;
