import React, { Component } from 'react';
import { Card, CardImg, CardBody, Button } from 'reactstrap'
class Page404 extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    navigateTo = (url) => {
        this.props.history.push("/");
    }
    render() {
        return (
            <React.Fragment>
                <div className="row m-0">
                    <div className="mt-3 mx-auto">
                        <Card>
                            <CardImg className="image-404" top src="/images/404.jpg" alt="404" />
                            <CardBody className="text-center">
                                <h4 className="mb-4">Page not found</h4>
                                <Button color="primary" onClick={() => this.navigateTo("/")}><i className="fas fa-home"></i> {" "}Home</Button>
                            </CardBody>
                        </Card>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default Page404;