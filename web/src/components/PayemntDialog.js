import React, { Component } from 'react';
class PaymentDialog extends Component {
    constructor(props) {
        super(props);
        this.state = {
            payment: {
                cardNumber: "",
                name: "",
                month: "",
                year: "",
                cvv: ""
            }
        }
    }

    handlePaymentChange = (event, value) => {
        let eve = { ...event }
        this.setState(prevState => ({
            payment: {
                ...prevState.payment,
                [eve.target.name]: value ? value : eve.target.value
            }
        }))
    }

    handleClose = (data) => {
        this.props.onClose(data);
    }

    handlePayment = (event) => {
        event.preventDefault();
        console.log(this.state.payment)
        this.handleClose(this.state.payment)
    }

    render() {
        return (
            <React.Fragment>
                <div className="modal" id="paymentDialog">
                    <div className="modal-dialog">
                        <div className="modal-content">

                            <div className="modal-header">
                                <h4 className="modal-title">Payment</h4>
                                <button type="button" className="close" onClick={() => this.handleClose()} data-dismiss="modal">&times;</button>
                            </div>

                            <div className="modal-body">
                                <form role="payment" onSubmit={this.handlePayment}>
                                    <div className="form-group">
                                        <label htmlFor="cardNumber">
                                            CARD NUMBER</label>
                                        <div className="input-group">
                                            <input onChange={this.handlePaymentChange} type="number" className="form-control" name="cardNumber" id="cardNumber" placeholder="Valid Card Number"
                                                required autoFocus />
                                            <span className="input-group-addon"><span className="glyphicon glyphicon-lock"></span></span>
                                        </div>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="name">
                                            Name on card</label>
                                        <div className="input-group">
                                            <input onChange={this.handlePaymentChange} type="text" className="form-control" name="name" id="name" placeholder="Name on card"
                                                required />
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-xs-7 col-md-7">
                                            <div className="form-group">
                                                <label htmlFor="expiryMonth">
                                                    EXPIRY DATE</label>
                                                <div className="d-flex">

                                                    <div className="col-xs-6 col-lg-6 pl-0">
                                                        <input onChange={this.handlePaymentChange} name="month" type="number" className="form-control" id="expiryMonth" placeholder="MM" required />
                                                    </div>
                                                    <div className="col-xs-6 col-lg-6 pl-0">
                                                        <input onChange={this.handlePaymentChange} type="number" name="year" className="form-control" id="expiryYear" placeholder="YY" required /></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-xs-5 col-md-5 pull-right">
                                            <div className="form-group">
                                                <label htmlFor="cvCode">
                                                    CVV CODE</label>
                                                <input onChange={this.handlePaymentChange} name="cvv" type="password" className="form-control" id="cvCode" placeholder="CVV" required />
                                            </div>
                                        </div>
                                        <div className="col-12">

                                            <button
                                                className="btn btn-lg btn-primary btn-block text-uppercase"
                                                type="submit"
                                            >
                                                Pay 50$
                    </button>
                                        </div>
                                    </div>
                                </form>
                            </div>

                            {/* <div className="modal-footer">
                                <button type="button" className="btn btn-danger" data-dismiss="modal">Close</button>
                            </div> */}

                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default PaymentDialog;