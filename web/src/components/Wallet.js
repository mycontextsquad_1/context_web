import React, { Component } from 'react';
import { ToastsContainer, ToastsStore, ToastsContainerPosition } from 'react-toasts';
import NavBar from './NavBar'
import CustomLoading from './Loading';
import { httpClient } from '../UtilService';
import PaymentModal from './PaymentModal'
class WalletDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            isDataLoaded: false,
            userDetails: {
                walletBalance: 0
            },
            walletBalance: "",
            paymentDialog: {
                isOpen: false,
            },
        }
    }
    componentDidMount() {
        document.title = "Wallet";
        this.getUserWalletDetails();
    }
    getUserWalletDetails = async () => {
        this.setState({
            isLoading: true
        })
        let result = await httpClient("user/wallet-details", "GET");
        if (result && result.success && result.data) {
            this.setState(prevState => ({
                isLoading: false,
                userDetails: {
                    ...prevState.userDetails,
                    ...result.data
                },
                walletBalance: "",
                paymentDialog: {
                    isOpen: false,
                },
                isDataLoaded: true,
            }))
        } else {
            this.setState({
                isLoading: false,
                isDataLoaded: true,
            })
        }
        console.log("result", result)
    }

    handleWalletUpdate = async () => {
        console.log("details", this.state.walletBalance);
        this.setState({
            isLoading: true
        })
        let result = await httpClient("user/wallet-details", "PATCH", { walletBalance: this.state.walletBalance });
        this.setState({
            isLoading: false
        })
        console.log("result", result)
        if (result && result.success) {
            ToastsStore.success("Wallet updated successfully Updated.");
            this.getUserWalletDetails();
        } else {
            ToastsStore.error("Error while updating.");
        }
    }

    handleChange = event => {
        let eve = { ...event };
        this.setState({
            walletBalance: eve.target.value
        });
    };


    handlePaymentDialogOpen = () => {
        if (!this.state.walletBalance || this.state.walletBalance < 0) {
            ToastsStore.error("Please enter valid amount");
            return
        }
        this.setState({
            paymentDialog: {
                isOpen: true,
            }
        });
    }

    handlePaymentDialogClose = async (data) => {
        if (data) {
            let self = this;
            self.setState({ loading: true });
            data.userId = localStorage.getItem("user_id");
            data.recordId = "wallet-recharge"
            var result = await httpClient("user/buy-record", "POST", data);
            console.log("payment ", result);
            if (result.success) {
                this.handleWalletUpdate()
            } else {
                self.setState({
                    loading: false,
                    paymentDialog: {
                        isOpen: false,
                    }
                });
                ToastsStore.error(result.message);
            }
        } else {
            this.setState({
                paymentDialog: {
                    isOpen: false,
                }
            })
        }
    }

    render() {
        return (
            <React.Fragment>
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                {this.state.isLoading && <CustomLoading />}
                <NavBar {...this.props} />
                <br />
                <div className="row m-0 mt-4">
                    <div className="col-sm-9 col-md-7 col-lg-5 mx-auto">
                        <br />
                        <div className="card">
                            <div className="card-body">
                                <form>
                                    <h5 className="text-center">Wallet</h5>
                                    <hr />
                                    <div className="row">
                                        <div className="col-md-6 col-12">
                                            <h5>Balance</h5>
                                            <h3>{this.state.userDetails.walletBalance}</h3>
                                        </div>
                                        <div className="col-md-6 col-12">
                                            {this.state.isDataLoaded && (
                                                <React.Fragment>
                                                    <div className="form-group">
                                                        <label htmlFor="walletBalance">Add Balance($)</label>
                                                        <input
                                                            id="walletBalance"
                                                            type="number"
                                                            className="form-control"
                                                            placeholder="Add Amount"
                                                            name="walletBalance"
                                                            value={this.state.walletBalance}
                                                            onChange={this.handleChange}
                                                            autoFocus
                                                        />
                                                    </div>
                                                    <div className="text-center mt-3">
                                                        <button type="button" className="btn btn-primary" onClick={this.handlePaymentDialogOpen}>Add Balance</button>
                                                    </div>
                                                </React.Fragment>
                                            )}
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.paymentDialog.isOpen && (
                    <PaymentModal selectedRecord={{ price: this.state.walletBalance }} onClose={this.handlePaymentDialogClose} />
                )}
            </React.Fragment>
        );
    }
}

export default WalletDetails;