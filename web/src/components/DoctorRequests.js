import React, { Component } from "react";
import NavBar from "./NavBar";
import {
    ToastsContainer,
    ToastsStore,
    ToastsContainerPosition
} from "react-toasts";

import { httpClient } from '../UtilService'
import CustomLoading from "./Loading";

class DoctorRequests extends Component {
    userId = ""
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            isLoading: true,
        };
    }

    componentDidMount() {
        document.title = "Doctor Requests";
        this.userId = localStorage.getItem("user_id")
        this.getDoctorRequests();
    }

    getDoctorRequests = async () => {
        var self = this;
        let result = await httpClient("user/doctor-request/patient", "GET");
        console.log(result);
        if (result.success) {
            self.setState({ data: result.data, isLoading: false });
        } else {
            this.setState({
                isLoading: false
            })
            ToastsStore.error(result.message);
        }
    }


    acceptDoctorRequest = async (data, index) => {
        let self = this;
        self.setState({ isLoading: true });

        var result = await httpClient("user/doctor-request/patient/accept", "PUT", data);
        if (result.success) {
            let data = [...this.state.data];
            data.splice(index, 1);
            this.setState({
                data,
                isLoading: false
            })
            ToastsStore.success("Accepted successfully");
        } else {
            self.setState({ isLoading: false });
            ToastsStore.error(result.message);
        }

    }

    render() {
        return (
            <React.Fragment>
                <ToastsContainer
                    store={ToastsStore}
                    position={ToastsContainerPosition.TOP_RIGHT}
                />
                <NavBar {...this.props} />
                <div className="container-fluid">
                    <div className="row">
                        <div
                            role="main"
                            className="col-12 main-container pt-60 mobile-space"
                        >
                            <h2>Doctor Requests</h2>
                            <hr />
                            {this.state.isLoading && (
                                <CustomLoading />
                            )}
                            <div className="table-responsive bg-white">
                                <table className="table table-bordered">
                                    <thead className="thead-light">
                                        <tr>
                                            <th scope="col">Doctor</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.data.map((request, index) => (
                                            <tr key={index}>
                                                <td>{request.doctorId.name}</td>
                                                <td>{request.doctorId.email}</td>
                                                <td>


                                                    <button onClick={() => this.acceptDoctorRequest(request)} type="button" className="btn btn-primary">
                                                        Accept
</button>

                                                </td>
                                            </tr>
                                        ))}
                                        {this.state.data.length === 0 && (
                                            <tr>
                                                <td className="text-center" colSpan="8">No requests found</td>
                                            </tr>
                                        )}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment >
        );
    }
}

export default DoctorRequests;
