import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Redirect, Switch } from "react-router-dom";
import "./css/index.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "popper.js";
import Login from "./components/Login";
import Register from "./components/Register";
import Home from "./components/Home";
import Records from "./components/Records";
import ViewRecord from "./components/ViewRecord";
import AddRecord from "./components/AddRecord";
import EditRecord from "./components/EditRecord";
import * as serviceWorker from "./serviceWorker";
import NewLoginPage from "./components/NewLoginPage";
import VerificationPage from "./components/VerificationPage";
import ConfirmEmailPage from "./components/ComfirmEmailPage";
import SignUpPage from "./components/SignUpPage";
import Page404 from "./components/Page404";
import ProfileDetails from "./components/ProfileDetails";
import AllUsers from "./components/AllUsers";
import PatientOffers from "./components/PatientOffers";
import Offers from "./components/Offers";
import WalletDetails from "./components/Wallet";
import RequestPatients from "./components/RequestPatients";
import DoctorRequests from "./components/DoctorRequests";

ReactDOM.render(
  <Router>
    <React.Fragment>
      <Switch>
        <Route exact path="/login" component={NewLoginPage} />
        <Route exact path="/signup" component={SignUpPage} />
        <Route exact path="/verify-email" component={VerificationPage} />
        <Route exact path="/confirm-email/:token" component={ConfirmEmailPage} />
        <Route exact path="/login-new" component={NewLoginPage} />
        <Route exact path="/register" component={Register} />
        <PrivateRoute exact path="/records" component={Records} />
        <PrivateRoute exact path="/" component={Home} />
        <PrivateRoute exact path="/add" component={AddRecord} />
        <PrivateRoute path="/view/:id" component={ViewRecord} />
        <PrivateRoute path="/edit/:id" component={EditRecord} />
        <PrivateRoute path="/profile-details" component={ProfileDetails} />
        <PrivateRoute path="/users" component={AllUsers} />
        <PrivateRoute path="/patient-offers" component={PatientOffers} />
        <PrivateRoute path="/request-patients" component={RequestPatients} />
        <PrivateRoute path="/doctor-requests" component={DoctorRequests} />
        <PrivateRoute path="/wallet" component={WalletDetails} />
        <PrivateRoute path="/offers" component={Offers} />
        <Route component={Page404}></Route>
      </Switch>
    </React.Fragment>
  </Router>,
  document.getElementById("root")
);

function PrivateRoute({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={props =>
        localStorage.getItem("access-token") ? (
          <Component {...props} />
        ) : (
            <Redirect
              to={{ pathname: "/login", state: { from: props.location } }}
            />
          )
      }
    />
  );
}

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
