//local
export const API_URL = "http://localhost:9000/"

//prod
// export const API_URL = "https://api-mycontext.herokuapp.com/"


export const CANCER_TYPES = [
    { label: "Mouth", value: "Mouth" },
    { label: "Bone", value: "Bone" },
    { label: "Skin", value: "Skin" },
    { label: "Stomach", value: "Stomach" },
    { label: "Throat", value: "Throat" },
    { label: "Thyroid", value: "Thyroid" },
    { label: "Prostate", value: "Prostate" },
    { label: "Lung", value: "Lung" },
    { label: "Liver", value: "Liver" },
    { label: "Brain", value: "Brain" },
    { label: "Breast", value: "Breast" },
    { label: "Kidney", value: "Kidney" },
    { label: "Pancreatic", value: "Pancreatic" },
];

export const USER_ROLES = [
    "Patient", "Doctor", "Hospital", "Pathologist", "Pharmaceutical Company", "Pathology Laboratory", "Insurance Company"
]

export const AGE_GROUPS = ["20-25", "26-30", "31-35", "36-40", "41-45", "46-50", "51-55", "56-60", "61-65", "66-70", "71-75", "76-80", "80+"]